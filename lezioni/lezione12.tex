\chapter{Lezione 12}

\section{Richiami della lezione precedente}

\`{E} possibile fornire una definizione alternativa di accettazione da parte di un automa di B\"{u}chi: definita una computazione $\rho = \{ q_{0}, q_{1}, \dots, q_{k}, \dots \}$ e l'insieme $Inf(\rho) = \{ q \in Q \mid q \mbox{ è visitato infinite volte}\}$ una parola $w \in \Sigma^{\omega}$ è accettata da un automa $\iff \exists \rho \mbox{ computazione per } w \ \ Inf(\rho) \cap F \neq \emptyset$. 

Un $\omega$-linguaggio si dice $\omega$-regolare se è accettato da un automa di B\"{u}chi non deterministico. Abbiamo per ora visto che la classe dei linguaggi $\omega$-regolari è chiusa per unione e intersezione, scopo della lezione di oggi è dimostrare che la classe è anche chiusa per complemento.

\bigskip
\textsf{Esercizio}:\\
Dati $L, J \subseteq \Sigma^{*}$ regolari dimostrare che anche il linguaggio $L \cdot J^{\omega}$ è regolare.

\bigskip
\textsf{Soluzione}:\\
Dati due automi $A_{1} = \langle Q_{1}, \Sigma, \delta_{1}, q_{0_{1}}, F_{1} \rangle$ e $A_{2} = \langle Q_{2}, \Sigma, \delta_{2}, q_{0_{2}}, F_{2} \rangle$ definisco un automa $A = \langle Q, \{a, b\}, \delta, q_{0}, F \rangle$ nel seguente modo:
\begin{itemize}
	\item $Q = Q_{1} \cup Q_{2}$
	\item $\delta(q, a) =
		\begin{cases}
			\delta_{2}(q, a), & \mbox{se } \delta_{2}(q, a) \in Q_{2} \setminus F_{2}\\
			\delta_{2}(q, a) \cup q_{0_{2}}, & \mbox{se } \delta_{2}(q, a) \in F_{2}\\
			\delta_{1}(q, a), & \mbox{se } \delta_{1}(q, a) \in Q_{1} \setminus F_{1}\\
			\delta_{1}(q, a) \cup q_{0_{2}}, & \mbox{se } \delta_{1}(q, a) \in F_{1}
		\end{cases}$
	\item $q_{0} =
		\begin{cases}
			q_{0_{1}}, & \mbox{se } q_{0_{1}} \notin F_{1}\\
			q_{0_{1}} \cup q_{0_{2}}, & \mbox{se } q_{0_{1}} \in F_{1}
		\end{cases}$
	\item $F = F_{2}$
\end{itemize}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=3.5cm]
		\node (start) {start};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=-1cm] (A_1) [right=of start] {$A_{1}$};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt] (A_2) [right=of A_1] {$A_{2}$};
		
		\path[->]
			(start)    edge node {} (A_1)
			(A_1)      edge node {$\epsilon$} (A_2);
			
		\draw[->] (A_2.east) .. controls (8.5, -1.7) and (3.5, -1.7) .. (A_2.west);
		\node at (6, -1.5) {$\epsilon$};
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per $L \cdot J^{\omega}$}}
\end{figure}

\section{Operazioni su linguaggi infiniti}
\subsection{Complemento}
Vediamo ora una costruzione nel caso non deterministico per l'automa del complemento di un linguaggio $\omega$-regolare.

Definiamo una relazione di equivalenza:
\[\forall x, y \in \Sigma^{*} \ x \equiv y \iff \forall p \in Q \ \delta(p, x) = \delta(p, y)\]
Con questa relazione posso sostituire tutte le occorrenze di un infisso $x$ con $y$ modificando la computazione solo nel tratto modificato: presenta però un problema, poiché se la computazione su $x$ visita uno stato finale ma la computazione su $y$ non visita uno stato finale potrei rifiutare stringhe che contengono infinite $x$ dopo averle sostituite con $y$.\\
Definiamo quindi una seconda relazione di equivalenza che sottolinea quindi questa necessità:
\[\forall x, y \in \Sigma^{*}\ x \equiv_{F} y \iff \forall p, q \in Q \ (p \xrightarrow{x}_{F} q \iff p \xrightarrow{y}_{F} q)\]

Posso considerare le classi di equivalenza associate:
\[ [x] = \{ y \in \Sigma^{*} \mid x \equiv_{F} y \} \]

Osserviamo che le classi di equivalenza associate sono finite in quanto il numero di stati è finito: in particolare per ogni parola $x \in \Sigma^{*}$ ci sono tre possibilità associate ad ogni coppia di stati:
\begin{itemize}
	\item non esiste una computazione da $p$ a $q$ su $x$
	\item esiste una computazione da $p$ a $q$ su $x$ che non passa per uno stato finale
	\item esiste una computazione da $p$ a $q$ su $x$ che passa per uno stato finale
\end{itemize}
Definendo quindi una funzione $Q \times Q \rightarrow \{1, 2, 3\}$ e contando il numero di queste funzioni so il numero delle classi di equivalenza esistenti ($3^{\#^{2}(Q)}$).

\newpage
\textsf{Proposizione}:\\
per ogni parola $x, y \in \Sigma^{*}$ ci sono due casi possibili:
\begin{itemize}
	\item $[x] \cdot [y]^{\omega} \subseteq L(A)$
	\item $[x] \cdot [y]^{\omega} \cap L(A) = \emptyset$ 
\end{itemize}

\bigskip
\textsf{Dimostrazione}:\\
considerate due parole $x_{0}y_{1}y_{2}\ldots, x_{0}'y_{1}'y'_{2}\ldots \in [x] \cdot [y]^{\omega}$ posso sostituire tuti gli infissi della seconda parola con gli infissi della prima parola grazie alla relazione di equivalenza $\Rightarrow$ le due parole sono entrambe accettate, oppure non lo è nessuna.

\bigskip
\textsf{Lemma}:\\
per ogni parola $w \in \Sigma^{\omega} \ \exists x, y \in \Sigma^{*} \ w \in [x] \cdot [y]^{\omega}$

\bigskip
\textsf{Teorema}:\\
le seguenti affermazioni sono vere per ogni linguaggio $\omega$-regolare:
\begin{itemize}
	\item $L(A) = \bigcup\limits_{\{([x], [y]) \mid [x] \cdot [y]^{\omega} \subseteq L(A)\}} [x]\cdot[y]^{\omega}$
	\item $L(A^{C}) = \bigcup\limits_{\{([x], [y]) \mid [x] \cdot [y]^{\omega} \cap L(A) = \emptyset\}} [x]\cdot[y]^{\omega}$
\end{itemize}

\bigskip
Avendo già visto precedentemente che i linguaggi regolari sono chiusi per unione e avendo dimostrato che se $L, J \subseteq \Sigma^{*}$ sono regolari allora $L \cdot J^{\omega}$ è $\omega$-regolare è sufficiente mostrare che le classi di equivalenza $[x]$ sono regolari per mostrare che i linguaggi $\omega$-regolari sono chiusi per complemento.
Costruiamo quindi un automa $A_{p, q, i}$ che riconosca le parole tali per cui la funzione vista prima applicata su $p, q$ restituisca il valore $i$.

%TODO: mostrare costruzione

Dati questi automi costruisco $A_{[x]} = \bigcap\limits_{p, q \in Q} A_{p, q, i}$ che riconosce la classe di equivalenz $[x]$: le classi di equivalenza sono quindi $\omega$-regolari, e quindi anche il complemento lo è.

\newpage
\section{Automi alternativi per linguaggi infiniti}
\textsf{Teorema}:\\
$L$ è $\omega$-regolare $\iff \exists I \mbox{ finito } \ \forall i \in I \  \exists U_{i}, V_{i} \subseteq \Sigma^{*} \mbox{ regolari } \ L = \bigcup\limits_{i \in I} U_{i}\cdot \overrightarrow{V_{i}}$

%TODO: mostrare costruzione

\subsection{Automa di Rabin}
Automa di Rabin: $\langle Q, \Sigma, \delta, q_{0}, \{(E_{1}, F_{1}), (E_{2}, F_{2}), \dots\} \rangle$ nel quale una parola $w \in \Sigma^{\omega}$ è accettata se $\exists \rho, i \ (Inf(\rho) \cap F_{i} \neq \emptyset \wedge Inf(\rho) \cap E_{i} = \emptyset)$

%TODO: aggiungere esempio visto a lezione