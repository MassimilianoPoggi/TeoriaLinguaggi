\chapter{Lezione 5}
\section{Conversione di automi}
\subsection{Da automi non deterministici ad automi deterministici}
\label{sect:lez5-nontodet}
Dato un automa non deterministico $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$, è possibile costruire un automa deterministico $A' = \langle Q', \Sigma, \delta', q_{0}', F' \rangle$ che in ogni stato ricorda l'insieme degli stati della computazione dell'automa non deterministico. Tale costruzione è chiamata \textit{per sottoinsiemi}:
\begin{itemize}
	\item $Q' \subseteq 2^{Q}$
	\item $\delta'$ simula l'esecuzione in parallelo di ogni configurazione possibile di $A$: a partire da ogni stato corrente, si applica la transizione opportuna. Formalmente:
		\[\forall q \in Q', a \in \Sigma \ \ \ \delta'(q, a) = \bigcup_{q_{i} \in q} \delta(q_{i}, a)\]
	\item $q_{0}' = \{ q_{0} \}$
	\item $F'$ contiene tutti gli stati che contengono almeno uno stato finale. Formalmente:
		\[\forall q \in Q' \ \ \ q \in F' \iff \exists q_{i} \in q : q_{i} \in F\]
\end{itemize}

Con questo tipo di costruzione, $\#Q' \leq 2^{\#Q}$: al più si prendono tutti i sottoinsiemi di $Q$.

\bigskip
\textsf{Esempio}:\\
per semplicità, si adotti la notazione $q_{x, y, \dots, k}$ per indicare il generico stato $\{ q_{x}, q_{y}, \dots, q_{k} \}$.
\begin{figure}[!ht]
	\centering
	\begin{tabular}{ccc}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state,initial] (q_0) {$q_{0}$};
				\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
				\path[->]
					(q_0) edge [loop above] node {$a$} (q_0)
					      edge [bend left=20] node {$a, b$} (q_1)
					(q_1) edge [bend left=20] node {$b$} (q_0)
					      edge [loop above] node {$b$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.04\textwidth}
			\begin{tikzpicture}
				$ \implies $
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.43\textwidth}
			\begin{tikzpicture}
				\node[state,initial] (q_0) {$q_{0}$};
				\node[state, accepting] (q_0q_1) [above right=of q_0] {$q_{0, 1}$};
				\node[state] (q_1) [below right=of q_0q_1] {$q_{1}$};
				\path[->]
					(q_0)    edge node {$a$} (q_0q_1)
					         edge [swap] node {$b$} (q_1)
					(q_0q_1) edge [loop above] node {$a, b$} (q_0q_1)
					(q_1)    edge [swap] node {$b$} (q_0q_1);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa non deterministico}&&
		\footnotesize{Automa deterministico}\\
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[->]
		\node {$q_{0}$}
			child { node [xshift=-0.5cm] {$q_{0}$} 
				child { node {$q_{1}$} 
					child { node [text=red] {$q_{0}$} }
					child { node [text=officegreen] {$q_{1}$} }
				}
			}
			child { node [xshift=0.5cm] {$q_{1}$}
				child { node {$q_{0}$} 
					child { node [text=officegreen] {$q_{1}$} }
				}
				child { node {$q_{1}$}
					child { node [text=red] {$q_{0}$} }
					child { node [text=officegreen] {$q_{1}$} }				
				}
			};

		\node at (4.25, -0.75) {$a$};
		\node at (4.25, -2.25) {$b$};
		\node at (4.25, -3.75) {$b$};

		\draw [rounded corners, color=blue] (-0.5, 0.25) rectangle (0.5, -0.25);
		\node at (4.25, 0) [text=blue] {$\{ q_{0} \}$};

		\draw [rounded corners, color=blue] (-1.75, -1.25) rectangle (1.75, -1.75);
		\node at (4.25, -1.5) [text=blue] {$\{ q_{0}, q_{1} \}$};

		\draw [rounded corners, color=blue] (-1.75, -2.75) rectangle (2.5, -3.25);
		\node at (4.25, -3) [text=blue] {$\{ q_{0}, q_{1} \}$};

		\draw [rounded corners, color=blue] (-2.5, -4.25) rectangle (3.25, -4.75);
		\node at (4.25, -4.5) [text=blue] {$\{ q_{0}, q_{1} \}$};
	\end{tikzpicture}
	\caption*{\footnotesize{Computazione sulla stringa $abb$}}
\end{figure}

\newpage
\textsf{Osservazioni}:
\begin{itemize}
	\item in generale, si possono costruire linguaggi per cui è possibile dimostrare che ogni automa deterministico che li riconosce deve avere almeno $2^{\#Q}$ stati
\end{itemize}

\bigskip
\textsf{Esempio}:\\
si prendano in considerazione i linguaggi del seguente tipo:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ l'$n$-esimo simbolo da destra è una } a\}\]

Si costruisca l'automa non deterministico associato:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state,initial] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node (q_i) [right=of q_1] {\dots};
		\node[state] (q_n-1) [right=of q_i] {$q_{n-1}$};
		\node[state, accepting] (q_n) [right=of q_n-1] {$q_{n}$};
		\path[->]
			(q_0)   edge [loop above] node {$a, b$} (q_0)
			        edge node {$a$} (q_1)
			(q_1)   edge node {$a, b$} (q_i)
			(q_i)   edge node {$a, b$} (q_n-1)
			(q_n-1) edge node {$a, b$} (q_n);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa non deterministico per $L_{n}$}}
\end{figure}

Tale automa non deterministico ha un totale di $n+1$ stati.
A fronte delle osservazioni rimarcate nella sezione \ref{sect:lez4-diff}, si è constatato che il corrispondente automa deterministico di $L_n$ richiederebbe almeno $2^{n}$ stati. In questo caso, nonostante il valore dell'upper bound non sia esattamente $n$ ma si avvicini molto (gli esempi da costruire sarebbero più complessi), è possibile notare che tale upper bound non è migliorabile.

Tuttavia, può capitare che in alcuni casi gli upper bound possano essere molto più alti rispetto al numero di stati effettivamente richiesti: si guardino ad esempio gli automi della sezione \ref{sect:lez4-es1}.

\newpage
\subsection[Da automi con \texorpdfstring{$\epsilon$}{e}-mosse ad automi non deterministici]{Da automi con $\bm{\epsilon}$-mosse ad automi non deterministici}
Una volta simulate tutte le sequenze di transizioni che contengono un simbolo e un numero arbitrario di $\epsilon$-mosse, vengono aggiunte le transizioni corrispondenti all'automa risultante.

\bigskip
\textsf{Esempio}:\\
si consideri il linguaggio $L = \{ a^{*}b^{*}a^{*}\}$:

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (q_0) {$q_{0}$}; 
		\node[state] (q_1) [right=of q_0] {$q_{1}$}; 
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge [loop below] node {$a$} (q_0)
			      edge node {$\epsilon$} (q_1)
			(q_1) edge [loop below] node {$b$} (q_1)
			      edge node {$\epsilon$} (q_2)
			(q_2) edge [loop below] node {$a$} (q_2);
	\end{tikzpicture}\\
	\caption*{\footnotesize{Automa con $\epsilon$-mosse}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial, accepting] (q_0) {$q_{0}$}; 
		\node[state] (q_1) [right=of q_0] {$q_{1}$}; 
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge [loop below] node {$a$} (q_0)
			      edge node {$a, b$} (q_1)
			      edge [above, bend left=60] node {$a, b$} (q_2)
			(q_1) edge [loop below] node {$b$} (q_1)
			      edge node {$a, b$} (q_2)
			(q_2) edge [loop below] node {$a$} (q_2);
	\end{tikzpicture}\\
	\caption*{\footnotesize{Automa non deterministico corrispondente}}
\end{figure}

Da quest'ultima rappresentazione è poi possibile ottenere una rappresentazione deterministica con la traduzione vista nella sezione \ref{sect:lez5-nontodet}.

\newpage
\section{Complemento sui linguaggi}
\label{sect:lez5-compl}
\textsf{Esempio 1}\\
Si consideri la seguente famiglia di linguaggi:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid x \mbox{ contiene due simboli diversi tra loro a distanza } n \}\]

Come si può riconoscere questo linguaggio mediante un automa deterministico?

Sfruttando il non determinismo, si indovini qual è il primo dei due simboli diversi:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state] (q_0) {$q_{0}$};
		\node[state] (q_1) [above right=of q_0] {$q_{1}$};
		\node (q_i) [right=of q_1] {\dots};
		\node[state] (q_n) [right=of q_i] {$q_{n}$};
		\node[state] (q_n+1) [below right=of q_0] {$q_{n+1}$};
		\node (q_j) [right=of q_n+1] {\dots};
		\node[state] (q_2n) [right=of q_j] {$q_{2n}$};
		\node[state, accepting] (q_f) [below right=of q_n] {$q_{f}$};
		\node (start) [above left=of q_0] {start};
		\path[->]
			(q_0)   edge [in=210, out=240, loop] node {$a, b$} (q_0)
			        edge node {$a$} (q_1)
			        edge [swap] node {$b$} (q_n+1)
			(q_1)   edge node {$a, b$} (q_i)
			(q_i)   edge node {$a, b$} (q_n)
			(q_n)   edge node {$b$} (q_f)
			(q_n+1) edge [swap] node {$a, b$} (q_j)
			(q_j)   edge [swap] node {$a, b$} (q_2n)
			(q_2n)  edge [swap] node {$a$} (q_f)
			(q_f)   edge [loop right] node {$a,b$} (q_f)
			(start) edge node {} (q_0);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa non deterministico per $L_{n}$}}
\end{figure}

Il numero di stati di un automa non deterministico così costruito è, in generale, $2n+2$.
Dunque, si può dimostrare che ciascun automa deterministico corrispondente necessita di almeno $2^{n}$ stati (salto esponenziale), considerando $X = \{a, b\}^{n}$ come insieme di stringhe distinguibili.

\bigskip
\textsf{Esempio 2}\\
Si consideri ora la seguente famiglia di linguaggi:
\[L'_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ tutti i simboli a distanza $n$ sono uguali}\} \]
Un modo alternativo per rappresentare il linguaggio, in relazione a \textsf{Esempio 1} è:
\[x \in L'_{n} \iff \exists w \in \{a, b\}^{n}, y \in Prefissi(w), k \in \mathbb{N} \ \ \ x = w^{k}y\]

Si noti che $L_{n}$ e $L'_{n}$ sono complementari.

Avendo un automa deterministico per $L_{n}$ è sufficiente fare il complemento degli stati finali (facendo attenzione ad includere l'eventuale stato trappola omesso) per ottenere un automa deterministico per $L'_{n}$:
è possibile quindi concludere che anche per $L'_{n}$ vale che ogni automa deterministico ha almeno $2^{n}$ stati.

\bigskip
\textsf{Attenzione}:\\
per un automa non deterministico, questo discorso non vale. Si può dimostrare che per $L'_{n}$ occorre un numero esponenziale di stati, anche per gli automi non deterministici (in questo caso, il non determinismo non aiuta).

\newpage
\section{Lower bound per automi non deterministici}
Si prenda in considerazione un linguaggio $L$ e un insieme di coppie di stringhe
\[P \subseteq \Sigma^{*} \times \Sigma^{*} = \{(x_{i}, y_{i}) \mid i \in [1, N]\}\]
L'insieme $P$ è detto \textit{fooling set} per $L$ se e solo se:
\begin{itemize}
	\item $\forall i \in [1, N]\ \ \ x_{i}y_{i} \in L$
	\item $\forall i, j \in [1, N]\ \ \ i \neq j \Rightarrow x_{i}y_{j} \notin L$
\end{itemize}
L'insieme $P$ è detto \textit{extended fooling set} per $L$ se e solo se:
\begin{itemize}
	\item $\forall i \in [1, N]\ \ \ x_{i}y_{i} \in L$
	\item $\forall i, j \in [1, N]\ \ \ i \neq j \Rightarrow x_{i}y_{j} \notin L \lor x_{j}y_{i} \notin L$
\end{itemize}

\textsf{Teorema}:\\
dato un linguaggio $L$ e $P$ \textit{extended fooling set} per $L$, allora ogni automa non deterministico per $L$ ha almeno $\#P$ stati.

\bigskip
\textsf{Dimostrazione}:\\
si supponga che $P = \{ (x_{i}, y_{i}) \mid i \in [1, N] \}$ e, per assurdo, che $A$ sia un automa non deterministico per $L$ con meno di $N$ stati.\\
Si faccia quindi girare $A$ su tutte le stringhe $x_{k}$: poiché ci sono meno di $N$ stati $\exists i, j \in [1, N] \ \ \ i \neq j : \delta(q_{0}, x_{i}) = \delta(q_{0}, x_{j})$.\\
Pertanto, continuando la computazione, $x_{i}y_{j} \in L \wedge x_{j}y_{i} \in L$, che porta ad un assurdo.

\bigskip
\textsf{Esempio}:\\
si utilizzi questo teorema per dimostrare che ogni automa non deterministico per i linguaggi $L'_{n}$ descritti nella sezione \ref{sect:lez5-compl} richiede almeno $2^{n}$ stati.

\bigskip
Si prendano in considerazione gli insiemi $P_{n} = \{ (x, x) \mid x \in \{a, b\}^{n} \}$.

Questi costituiscono un \textit{fooling set} per $L'_{n}$. Infatti:
\begin{itemize}
	\item $x_{i}x_{i} \in L'_{n}$
	\item $x_{i}x_{j} \notin L'_{n}$
\end{itemize}
Essendo $\#P_{n} = 2^{n}$, ogni automa non deterministico per $L'_{n}$ richiede almeno $2^{n}$ stati.

\bigskip
\textsf{Osservazione}:\\
analogamente al caso osservato nella sezione \ref{sect:lez5-compl}, anche in questo caso  il non determinismo non aiuta ad avere meno stati.

\newpage
\section{Esercizi a casa}
Dato il linguaggio (finito):
\[L_{n} = \{ 0^{i}1^{i}2^{i} \mid i \in [0, n] \}\]
costruire per prima cosa un automa non deterministico e un automa deterministico corrispondenti. Poi trovare il lower bound per gli stati.

\bigskip
\textsf{Soluzione}:

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state,initial, accepting] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node[state] (q_2) [right=of q_1, xshift=\getnodedistance+\getnodedistance] {$q_{2}$};

		\node[state] (q_3) [above=of q_1] {$q_{3}$};
		\node[state] (q_5) [above=of q_2] {$q_{5}$};
		\node[state] (q_4) at  ($(q_3)!0.5!(q_5)$) {$q_{4}$};

		\node[state] (q_6) [above=of q_3] {$q_{6}$};
		\node[state] (q_7) [right=of q_6] {$q_{7}$};
		\node[state] (q_8) [right=of q_7] {$q_{8}$};
		\node[state] (q_9) [right=of q_8] {$q_{9}$};

		\node[state, accepting] (q_f) [right=of q_2] {$q_{f}$};
		\path[->]
			(q_0) edge [pos=0.15] node {$0$} (q_1)
			(q_1) edge node {$1$} (q_2)
			      edge node {$0$} (q_3)
			(q_2) edge [pos=0.15] node {$2$} (q_f)
			(q_3) edge node {$1$} (q_4)
			      edge node {$0$} (q_6)
			(q_4) edge node {$1$} (q_5)
			(q_5) edge node {$2$} (q_2)
			(q_6) edge [pos=0.15] node {$1$} (q_7)
			(q_7) edge node {$1$} (q_8)
			(q_8) edge [pos=0.15] node {$1$} (q_9)
			(q_9) edge node {$2$} (q_5);

		\draw [color=blue] (1.1,-1) rectangle (2.9,5);
		\draw [color=blue] (7.1,-1) rectangle (8.9,5);
		\draw [color=red] (3.1,-1) rectangle (6.9, 5);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico per $L_{3}$}}
\end{figure}

Generalizzando per un $n$ qualsiasi:
\begin{itemize}
	\item le porzioni in blu sono costituite da $n$ nodi ciascuna
	\item la porzione in rosso è costituita da $\sum\limits_{i=0}^{n-1} i = \frac{n(n-1)}{2}$ nodi
	\item esiste sempre uno stato iniziale, uno stato finale (tranne che per $n = 0$) ed uno stato trappola
\end{itemize}
Pertanto il numero di stati di un automa non deterministico per $L_{n}$ con questa costruzione è, in generale, pari a $\frac{n(n+3)}{2} + 3$, tranne per $n = 0$, caso in cui il numero di stati è pari a 2. Si noti inoltre che per passare da un automa che riconosce $L_{n-1}$ ad uno che riconosce $L_{n}$ è sufficiente aggiungere $n+1$ stati.

\bigskip
\textsf{Osservazione}:\\
in questa situazione, il non determinismo non è d'aiuto. Infatti è un problema di conteggio, per cui non dev'essere compiuta alcuna scelta.
L'automa risultante è conseguentemente identico a quello deterministico.

\newpage
Si cerchi ora di dimostrare l'ottimalità dell'automa generato costruendo per induzione un insieme di stringhe distinguibili pari al numero di stati dell'automa:
\begin{itemize}
	\item $n = 0$: $L_{0} = \{ \epsilon \}$. Considerata la stringa $\epsilon$ e qualsiasi altra stringa, si può notare che esse sono distinguibili, data $z = \epsilon$. Si osservi inoltre che due stringhe qualsiasi diverse da $\epsilon$ non sono distinguibili: considerato quindi $X_{0} = \{ \epsilon, 0 \}$, è possibile concludere che 2 è un lower bound massimale per il numero di stati di un automa deterministico che riconosce $L_{0}$.
	\item $n = 1$: $L_{1} = \{ \epsilon, 012 \}$. Si osservi che le due stringhe di $L_{1}$ sono distinguibili fra loro con $z = 012$ e che sono entrambe distinguibili da qualsiasi stringa non appartenente a $L_{1}$ con $z = \epsilon$. Poi si prendano in considerazione $Prefissi(012) = \{ \epsilon, 0, 01, 012 \}$: i prefissi stretti sono tutti distinguibili (è sufficiente usare il suffisso che completa la stringa $012$) da $\epsilon$ e $012$ e anche da qualsiasi altra stringa non appartenente a $L_{1}$. Considerato quindi l'insieme $X_{1} = \{ \epsilon, 0, 01, 012, 0012 \}$, è possibile concludere che 5 è un lower bound massimale per il numero di stati di un automa deterministico che riconosce $L_{1}$.
	\item $n = k$ con $k \geq 2$: $L_{k} = L_{k-1} \cup \{ 0^{k}1^{k}2^{k} \}$. Come nel caso precedente, si prendano in esame $Prefissi(0^{k}1^{k}2^{k})$, osservando che:
		\begin{itemize}
			\item $\epsilon \in X_{k-1}$
			\item i seguenti prefissi sono già contenuti: $\{0^{i} \mid i \in [1, k-1]\} \subset X_{k-1}$
			\item i seguenti prefissi sono indistinguibili da stringhe in $X_{k-1}$: \[\{0^{k}1^{k}2^{i+1} \mid i \in [0, k-1]\}\]
			\item i prefissi $Y_{k} = \{0^{k}1^{i} \mid i \in [0, k]\}$ sono distinguibili da qualsiasi stringa in $X_{k-1}$ per $z = 1^{k-i}2^{k}$
		\end{itemize}
		Si costruisca quindi $X_{k} = X_{k-1} \cup Y_{k}$.\\
		Si osservi che, essendo $\#Y_{k} = k+1$, abbiamo dimostrato il risultato desiderato: all'aumentare di $n$ il numero di stati cresce di $n+1$.
\end{itemize}

Per gli automi non deterministici è possibile costruire un extended fooling set $P_{n}$ in maniera quasi analoga, escludendo solo la stringa non appartenente al linguaggio (0012):
\[P_{n} = \{(x_{i}, y_{i}) \mid x_{i} \in (X_{n} \setminus \{0012\}) \wedge x_{i}y_{i} \in L_{n}\}\]
Si osservi che questo è un extended fooling set, in quanto le stringhe $y_{i}$ sono le $z$ utilizzate precedentemente per dimostrare la distinguibilità delle $x_{i}$.\\
Essendo $\#P_{n} = \#X_{n} - 1$, si è dimostrato che non è possibile ridurre il numero di stati dell'automa deterministico in maniera significativa: viene infatti rimosso solo lo stato trappola.

\bigskip
Se non si vuole utilizzare l'induzione si può in alternativa utilizzare la seguente costruzione (l'insieme risultante è il medesimo eccetto la coppia $(012, \epsilon)$):
\[P_{n} = \{ (0^{i}1^{j}, 1^{i-j}2^i) \mid 0 \leq j \leq i \leq k\}\]

Si osservi che questo costituisce effettivamente un fooling set:

\[\forall (i, j), (i', j') \in [0, k] \times [0, k] \ \ \ (i, j) \neq (i', j') \Rightarrow 0^{i}1^{j}1^{i'-j'}2^{i'} \notin L_{n}\]

Infatti $i \neq i'$ porta le sequenze di 0 e di 2 ad avere lunghezza diversa, mentre $i = i' \ \wedge \ j \neq j'$ porta le sequenze di 1 ad avere lunghezza diversa dalle sequenze di 0 e 2.

Infine si calcoli la cardinalità dell'extended fooling set appena individuato:
\[\#P_{n} = \sum_{i=0}^{k}(i-1) = \frac{k(k+3)}{2} \color{red}+ 1\]

\`E possibile notare una leggera perdita di precisione (uno stato) rispetto all'extended fooling set trovato per induzione: infatti quello ricavato esplicitamente (essendo un fooling set) include la coppia $(\epsilon, \epsilon)$, mentre quello ricavato per induzione (che è un extended fooling set) contiene al suo posto le coppie $(\epsilon, 012)$ e $(012, \epsilon)$.

Questo è dovuto al fatto che nella costruzione per induzione è stato esplicitamente separato il caso $n = 0$ dal processo induttivo, in quanto è l'unico caso in cui lo stato iniziale e lo stato finale coincidono.
