\chapter{Lezione 2}
\section{Definizioni basilari}
Innanzitutto, per poter passare agevolmente alla trattazione di concetti composti o in generale più elaborati, occorre fondare le basi del discorso. Per questo motivo, si passino in rassegna simbologia e definizioni che verranno usate da qui in avanti:
\begin{itemize}
	\item $\Sigma$: alfabeto (insieme finito di simboli) (eg: $\Sigma = \{a, b\}$)
	\item $x \in \Sigma^{*}$: parola, ovvero una sequenza finita di simboli appartenenti all'alfabeto $\Sigma$ (eg: $aab \in \{a, b\}^{*}$)
	\item $\epsilon$: parola vuota (costituita da 0 simboli)
	\item |\textit{x}|: numero di simboli che costituiscono la parola \textit{x} (eg: $|aab| = 3$)
	\item $\charcount{a}{x}$: numero di occorrenze del simbolo \textit{a} in \textit{x} (eg: $\charcount{b}{aab} = 1$)

	\item $x * y$: prodotto/concatenazione di parole (eg: $aab * aab = aabaab$)

	\item \textit{prefisso} di una parola $x \in \Sigma$ ($\#Prefissi(x) = |x| + 1$): una qualunque parte iniziale di \textit{x}. Formalmente:
		\[Prefissi(x) = \{y \in \Sigma^{*} \mid \exists z \in \Sigma^{*} : x = yz\}\]
	\item \textit{suffisso} di una parola $x \in \Sigma$ ($\#Suffissi(x) = |x| + 1$): una qualunque parte finale di \textit{x}. Formalmente:
		\[Suffissi(x) = \{y \in \Sigma^{*} \mid \exists z \in \Sigma^{*} : x = zy\}\]
	\item \textit{fattore}, o \textit{infisso}, di una parola $x \in \Sigma$ ($\#Fattori \sim \frac{n^2}{2}$): una qualunque parte della parola \textit{x}. Formalmente:
		\[Fattori(x) = \{y \in \Sigma^{*} \mid \exists w, z \in \Sigma^{*} : x = wyz\}\]
	\item \textit{sottoparola} (o \textit{sottostringa}, a seconda del contesto): una sequenza di caratteri, non necessariamente contigui. Posso ottenere una sottoparola rimuovendo una quantità arbitraria di simboli dalla parola iniziale (eg: $ac$ è una sottoparola di $aabc$)

	\item $L \subseteq \Sigma^{*}$: linguaggio, ovvero un sottoinsieme di tutte le parole costruite sull'alfabeto $\Sigma$ (eg: $\{ aa, ab, ba, bb \} \subset \{a, b\}^{*}$ è il linguaggio delle parole di lunghezza 2 sull'alfabeto $\{a, b\}$)
	\item $\emptyset$: linguaggio vuoto
	\item $L = \Sigma^{*}$: linguaggio pieno/completo	
\end{itemize}

\section{Rappresentazioni di un linguaggio}
Un linguaggio può avere tre rappresentazioni:
\begin{itemize}
	\item dichiarativa: viene descritta una proprietà che caratterizza tutte le parole del linguaggio
	\item riconoscitiva: viene fornita una macchina che, data una parola, risponde nel seguente modo
		\[ M(x) =
			\begin{cases}
				\mbox{Sì}, & \mbox{se } x \in L \\
				\mbox{No} \lor \bot, & \mbox{se } x \notin L \\
			\end{cases}
		\]
	\item generativa: viene dato un processo per generare tutte le stringhe del linguaggio
\end{itemize}

\textsf{Esempio}:\\
si consideri l'alfabeto $\Sigma = \{(,\ )\}$ e il linguaggio $L$ che contiene tutte le parole su $\Sigma$ con parentesi ben bilanciate.
\begin{itemize}
	\item Definizione generativa (il linguaggio risultante sarà $L_{G}$):
		\begin{itemize}
			\item $\epsilon \in L_{G}$
			\item $\forall x \in L_{G} \ (x) \in L_{G}$
			\item $\forall x,y \in L_{G} \ xy \in L_{G}$
		\end{itemize}

	\item Definizione riconoscitiva (il linguaggio risultante sarà $L_{R}$):
		\[x \in L_{R} \iff \charcount{(}{x} = \charcount{)}{x} \wedge \forall y \in Prefissi(x) \ \charcount{(}{y} \geq \charcount{)}{y}\]
\end{itemize}

\newpage
\textsf{Domanda}: $L_{G} \stackrel{?}{=} L_{R} $

Generalmente, in questi casi, per dimostrare l'uguaglianza tra due linguaggi si prova prima l'inclusione in un verso ($\subseteq$), poi nell'altro ($\supseteq$).

Si dimostri dunque $L_{G} \subseteq L_{R}$. Per induzione:
\begin{itemize}
	\item $\epsilon \in L_{R}$ perché $\charcount{(}{\epsilon} = \charcount{)}{\epsilon} = 0$ e $Prefissi(\epsilon) = \{ \epsilon \}$
	\item $x = ( x' ) \in L_{G}$ con $x' \in L_{G} \wedge x' \in L_{R}$ per ipotesi induttiva:
		\begin{itemize}
			\item $\charcount{(}{x} = 1 + \charcount{(}{x'} = \charcount{)}{x'} + 1 = \charcount{)}{x}$
			\item $Prefissi(x) = \{ \epsilon, (, (x', (x') \}$
				\begin{itemize}
					\item $\charcount{(}{(} = 1 > 0 = \charcount{)}{(}$
					\item $\charcount{(}{(x'} = 1 + \charcount{(}{x'} >	\charcount{)}{x'} = \charcount{)}{(x'}$
				\end{itemize}
		\end{itemize}
	\item $x = yz \in L_{G} \mbox{ con } y,z \in L_{G} \wedge y,z \in L_{R}$ per ipotesi induttiva:
		\begin{itemize}
			\item $\charcount{(}{x} = \charcount{(}{y} + \charcount{(}{z} = \charcount{)}{y} + \charcount{)}{z} = \charcount{)}{x}$
			\item Si individuano due categorie di prefissi:
				\begin{itemize}
					\item Prefissi contenuti interamente in y: si applica direttamente l'ipotesi induttiva
					\item Prefissi che contengono interamente y più un prefisso di z (chiamato qui $z'$):\\
						$\charcount{(}{yz'} = \charcount{(}{y} + \charcount{(}{z'} \geq \charcount{)}{y} + \charcount{)}{z'} = \charcount{)}{yz'}$
				\end{itemize}
		\end{itemize}
\end{itemize}
Si dimostri ora che $L_{G} \supseteq L_{R}$:
%TODO: proof
\newpage
\section{Grammatiche}
Una grammatica $G$ è una quadrupla $\langle V, \Sigma, P, S \rangle$\ dove:
\begin{itemize}
	\item $V$ è l'insieme dei simboli variabili o non-terminali
	\item $\Sigma$ è l'insieme dei simboli terminali
	\item $P$ è l'insieme delle produzioni, dove una produzione è definita come $\alpha \to \beta$ con $\alpha \in (V \cup \Sigma)^{+}, \beta \in (V \cup \Sigma)^{*}$
	\item $S \in V$ è detto assioma o simbolo iniziale	
\end{itemize}

\textsf{Esempio}:\\
la grammatica per espressioni con parentesi ben bilanciate è la seguente:
\begin{itemize}
	\item $V = \{ S \}$
	\item $\Sigma = \{ (,\ ) \}$
	\item $P = \{ S \to \epsilon, S \to (S), S \to SS\}$
	\item $S = S$
\end{itemize}

\textsf{Osservazione}:\\
la grammatica è una variante della rappresentazione generativa mostrata precedentemente.

\subsection{Regole di derivazione}
Date due stringhe $x, y \in (V \cup \Sigma)^{*}$ sono definite le seguenti regole di derivazione:
\begin{itemize}
	\item derivazione in un passo:
		\[x \Rightarrow y \iff \exists \eta, \alpha, \beta, \delta \in (V \cup \Sigma)^{*} :\]\[ x = \eta \alpha \delta \wedge y = \eta \beta \delta \wedge \alpha \to \beta \in P\]
	\item derivazione in k passi:
		\[x \xRightarrow{k} y \iff \exists x_{0}, ..., x_{k} \in (V \cup \Sigma)^{*} :\]\[ x = x_{0} \wedge y = x_{k} \wedge \forall i \in \mbox{[1, k] } x_{i-1} \Rightarrow x_{i}\]
	\item derivazione in un numero indefinito di passi:
		\[x \xRightarrow{*} y \iff \exists k \geq 0 : x \xRightarrow{k} y\]
\end{itemize}

Con queste regole di derivazione, è possibile definire il linguaggio derivato da una grammatica:
\[L_{G} = \{ x \in \Sigma^{*} \mid S \xRightarrow{*} x \} \]

\textsf{Esempio}:\\
ecco la derivazione di (()()) con la grammatica delle espressioni con parentesi ben bilanciate:
\[S \Rightarrow (S) \Rightarrow (SS) \Rightarrow (S(S)) \Rightarrow ((S)(S)) \Rightarrow (()(S)) \Rightarrow (()())\]

\section{Esempio articolato}
In vista dei concetti appena affrontati, si prenda in considerazione un esempio di grammatica più articolato. In particolare, la grammatica in questione esprime il linguaggio delle parole ripetute due volte:
\[L = \{ ww \mid w \in \{a, b\}^{*}\}\]

\begin{center}
	\begin{prodlist}[Insieme delle produzioni della grammatica di \textit{L}]{3}
		\production{S}{ABC}
		\production{AB}{aAD}
		\production{AB}{bAE}
		\production{DC}{BaC}
		\production{EC}{BbC}
		\production{Da}{aD}
		\production{Db}{bD}
		\production{Ea}{aE}
		\production{Eb}{bE}
		\production{AB}{\epsilon}
		\production{C}{\epsilon}
		\production{aB}{Ba}
		\production{bB}{Bb}
	\end{prodlist}
\end{center}
Per esercizio, si provi a ricavare la derivazione della stringa \textit{aabaab}.

\textsf{Notazione}:
\begin{itemize}
	\item informalmente, verrà indicata con $x \xRightarrow[(n)]{} y$ la derivazione ottenuta utilizzando la produzione di indice \textit{n}
	\item ad ogni passaggio verrà sottolineata la parte della stringa che si va a sostituire con la derivazione successiva
\end{itemize}

Perciò:\\
$\underline{S} \xRightarrow[(1)]{} \underline{AB}C \xRightarrow[(2)]{} aA\underline{DC} \xRightarrow[(4)]{} a\underline{AB}aC \xRightarrow[(2)]{} aaA\underline{Da}C \xRightarrow[(6)]{} aaAa\underline{DC} \xRightarrow[(4)]{}$\\[7pt]
$aaA\underline{aB}aC \xRightarrow[(4)]{} aa\underline{AB}aaC \xRightarrow[(3)]{} aabA\underline{Ea}aC \xRightarrow[(8)]{} aabAa\underline{Ea}C \xRightarrow[(8)]{}$\\[7pt]
$aabAaa\underline{EC} \xRightarrow[(5)]{} aabAa\underline{aB}bC \xRightarrow[(12)]{} aabA\underline{aB}abC \xRightarrow[(12)]{} aab\underline{AB}aabC \xRightarrow[(10)]{}$\\[7pt] $aabaab\underline{C} \xRightarrow[(11)]{} \boldsymbol{aabaab}$

\section{Esercizio a casa}
Che linguaggio genera il seguente insieme di produzioni?

\begin{center}
	\begin{prodlist}{3}
		\production{S}{ACaB}
		\production{Ca}{aaC}
		\production{CB}{DB}
		\production{CB}{E}
		\production{aD}{Da}
		\production{AD}{AC}
		\production{aE}{Ea}
		\production{AE}{\epsilon}
	\end{prodlist}
\end{center}
\vspace{-3mm}
\textsf{Svolgimento}:

$\underline{S} \xRightarrow[(1)]{} A\underline{Ca}B \xRightarrow[(2)]{} Aaa\underline{CB} \xRightarrow[(4)]{} Aa\underline{aE} \xRightarrow[(7)]{} A\underline{aE}a \xRightarrow[(7)]{} \underline{AE}aa \xRightarrow[(8)]{} \boldsymbol{aa}$

\hrulefill

$\underline{S} \xRightarrow[(1)]{} A\underline{Ca}B \xRightarrow[(2)]{} Aaa\underline{CB} \xRightarrow[(3)]{} Aa\underline{aD}B \xRightarrow[(5)]{} A\underline{aD}aB \xRightarrow[(5)]{} \underline{AD}aaB \xRightarrow[(6)]{}$\\[7pt]
$A\underline{Ca}aB \xRightarrow[(2)]{} Aaa\underline{Ca}B \xRightarrow[(2)]{} Aaaaa\underline{CB} \xRightarrow[(4)]{} Aaaa\underline{aE} \xRightarrow[(7)]{} Aaa\underline{aE}a \xRightarrow[(7)]{}$ \\[7pt]
$Aa\underline{aE}aa \xRightarrow[(7)]{} A\underline{aE}aaa \xRightarrow[(7)]{} \underline{AE}aaaa \xRightarrow[(8)]{} \boldsymbol{aaaa}$

\hrulefill

$\underline{S} \xRightarrow[(1)]{} A\underline{Ca}B \xRightarrow[(2)]{} Aaa\underline{CB} \xRightarrow[(3)]{} Aa\underline{aD}B \xRightarrow[(5)]{} A\underline{aD}aB \xRightarrow[(5)]{} \underline{AD}aaB \xRightarrow[(6)]{}$\\[7pt]
$A\underline{Ca}aB \xRightarrow[(2)]{} Aaa\underline{Ca}B \xRightarrow[(2)]{} Aaaaa\underline{CB} \xRightarrow[(3)]{} Aaaa\underline{aD}B \xRightarrow[(5)]{}$\\[7pt]
$Aaa\underline{aD}aB \xRightarrow[(5)]{} Aa\underline{aD}aaB \xRightarrow[(5)]{} A\underline{aD}aaaB \xRightarrow[(5)]{} \underline{AD}aaaaB \xRightarrow[(6)]{}$\\[7pt]
$A\underline{Ca}aaaB \xRightarrow[(2)]{} Aaa\underline{Ca}aaB \xRightarrow[(2)]{} Aaaaa\underline{Ca}aB \xRightarrow[(2)]{}$\\[7pt]
$Aaaaaaa\underline{Ca}B \xRightarrow[(2)]{} Aaaaaaaaa\underline{CB} \xRightarrow[(4)]{} Aaaaaaaa\underline{aE} \xRightarrow[(7)]{}$\\[7pt]
$Aaaaaaa\underline{aE}a \xRightarrow[(7)]{} Aaaaaa\underline{aE}aa \xRightarrow[(7)]{} Aaaaa\underline{aE}aaa \xRightarrow[(7)]{}$\\[7pt]
$Aaaa\underline{aE}aaaa \xRightarrow[(7)]{} Aaa\underline{aE}aaaaa \xRightarrow[(7)]{} Aa\underline{aE}aaaaaa \xRightarrow[(7)]{}$\\[7pt]
$A\underline{aE}aaaaaaa \xRightarrow[(7)]{} \underline{AE}aaaaaaaa \xRightarrow[(8)]{} \boldsymbol{aaaaaaaa}$

\hrulefill

\dots

\bigbreak
\textsf{Osservazione}:

tra tutte le produzioni possibili, solo da $CB$ è possibile compiere due scelte, ottenendo $E$ (4) oppure $DB$ (3). Nel primo caso le derivazioni conducono a terminare la produzione della stringa, mentre nel secondo caso viene gradualmente riportata la $D$ all'inizio della stringa; quindi, applicando la produzione $AD$, viene raddoppiato il numero di $a$ presenti nella stringa, ritornando infine ad avere disponibile la produzione $CB$, per cui valgono le considerazioni appena enucleate.
\bigbreak
\textsf{Soluzione}:
\[L = \{x \in \{a\}^{*} \mid \exists n \in \mathbb{N}^{+} : |x| = 2^n\}\]
