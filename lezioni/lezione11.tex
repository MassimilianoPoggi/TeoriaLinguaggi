\chapter{Lezione 11}
\section{Linguaggi di parole infinite}
Consideriamo ora un'estensione dei linguaggi in cui le parole hanno un infinito numero di simboli: una parola $w$ di infiniti simboli appartiene all'insieme $\Sigma^{\omega}$ e si indica quindi un linguaggi infinito $L \subseteq \Sigma^{\omega}$.

Posso simulare l'esecuzione di una parola infinita su un automa definito come nel caso finito, ma cambiando la condizione di accettazione: un automa accetta una stringa di lunghezza infinita se e solo se durante la computazione visita un numero infinito di volte uno stato finale. Questo tipo di automa è detto automa di B\"{u}chi.

\bigskip
\textsf{Esempi}:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (p) {$p$};
		\node[state] (q) [right=of p] {$q$};
		\path[->]
			(p) edge [loop above] node {$a$} (p)
			    edge [bend left=20] node {$b$} (q)
			(q) edge [bend left=20] node {$b$} (p)
			    edge [loop above] node {$a$} (q);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico che accetta parole con un numero di $b$ pari o infinito}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (p) {$p$};
		\node[state] (q) [right=of p] {$q$};
		\path[->]
			(p) edge [loop above] node {$a,b$} (p)
			    edge [bend left=20] node {$c$} (q)
			(q) edge [bend left=20] node {$b$} (p)
			    edge [loop above] node {$a,c$} (q);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico che accetta parole che hanno una $b$ dopo ciascuna $c$}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (p) {$p$};
		\node[state] (q) [right=of p] {$q$};
		\path[->]
			(p) edge [loop above] node {$a$} (p)
			    edge [bend left=20] node {$b$} (q)
			(q) edge [bend left=20] node {$a$} (p)
			    edge [loop above] node {$b$} (q);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico che accetta parole con un numero infinito di $a$}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (p) {$p$};
		\node[state] (q) [right=of p] {$q$};
		\path[->]
			(p) edge [bend left=20] node {$a,b$} (q)
			(q) edge [bend left=20] node {$a$} (p)
			    edge [loop above] node {$b$} (q);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico che accetta parole con un numero infinito di $a$}}
\end{figure}
\newpage
Si noti che gli ultimi due automi visti accettano lo stesso linguaggio, sono deterministici e sono minimali.

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (p) {$p$};
		\node[state, accepting] (q) [right=of p ]{$q$};
		\path[->]
			(p) edge [loop above] node {$a,b$} (p)
			    edge node {$a$} (q)
			(q) edge [loop above] node {$a$} (q);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa non deterministico che accetta parole con un numero finito di $b$}}
\end{figure}

Questo linguaggio non è però riconoscibile con un automa di B\"{u}chi deterministico.

\bigskip
\textsf{Dimostrazione}:\\
supponiamo per assurdo che esista un automa $A = \langle Q, \{a, b\}, \delta, q_{0}, F \rangle$ deterministico per riconoscere il linguaggio appena visto.\\
La parola $ba^{\omega}$ appartiene al linguaggio, pertanto visita un numero di volte infinito uno stato finale: consideriamo in particolare la prima volta che viene visitato uno stato finale: possiamo quindi dividere la stringa e scriverla come $ba^{n_{1}}a^{\omega}$ dove $\delta(q_{0}, ba^{n_{1}}) \in F$.\\
Allo stesso modo anche $ba^{n_{1}}ba^{\omega}$ appartiene al linguaggio, e posso quindi svolgere lo stesso ragionamento per dividere la stringa in $ba^{n_{1}}ba^{n_{2}}a^{\omega}$.\\
Iterando continuamente il ragionamento arrivo ad una sitauzione del tipo $ba^{n_{1}}ba^{n_{2}}\dots ba^{n_{k}}a^{\omega}$ in cui sono stati visitati $k$ stati finali dell'automa $A$.\\
Poiché gli stati dell'automa sono finiti (e potendo ripetere il processo un numero arbitrario di volte) prima o poi troverò lo stesso stato finale in due posizioni diverse della stringa: ho quindi trovato un ciclo, che posso pertanto iterare infinite volte.\\
Così facendo l'automa arriva però ad accettare una stringa che contiene la lettera $b$ un numero infinito di volte: sono arrivato ad un assurdo.

\newpage
\textsf{Osservazioni}:\\
Negli esempi abbiamo pertanto osservato le seguenti caratteristiche in cui gli automi di Buchi differiscono dai tradizionali automi:
\begin{itemize}
	\item l'automa di B\"{u}chi deterministico minimo non è unico
	\item gli automi di B\"{u}chi non deterministici hanno una potenza computazionale superiore agli automi di Buchi deterministici
\end{itemize}

\bigskip
Dato un linguaggio $L$ di parole finite definiamo la sua estensione infinita $\overrightarrow{L}$:
\[\overrightarrow{L} = \{w \in \Sigma^{\omega} \mid \exists \mbox{ un numero infinito di prefissi di $w \in L$} \}\]

\textsf{Teorema}:\\
dato un automa deterministico $A = \langle Q, \{a, b\}, \delta, q_{0}, F \rangle$ indico con $L_{B}(A)$ il linguaggio infinito e con $L_{F}(A)$ il linguaggio finito ad esso associati.
\[L_{B}(A) = \overrightarrow{L_{F}(A)} \]
\textsf{Corollario}:\\
$L \subseteq \Sigma^{\omega}$ è accettato da un automa di B\"{u}chi deterministico se e solo se esiste un linguaggio regolare $J \subseteq \Sigma$ tale che $L = \overrightarrow{J}$.

\section{Operazioni su linguaggi infiniti}
\subsection{Unione}
L'unione di linguaggi infiniti si può fare con le stesse costruzioni viste nella sezione \ref{sect:lez6-union} per i linguaggi finiti.

\subsection{Intersezione}
\label{sect:lez11-intersect}
L'idea nel caso dell'intersezione è simile a quella vista nella sezione \ref{sect:lez6-intersect}: non è possibile però applicare la costruzione direttamente in quanto è possibile che le due computazioni non siano sincronizzate e visitino gli stati finali in momenti diversi della computazione.\\
Creiamo quindi due automi prodotto, ciascuno dei quali segue la computazione di uno dei due automi e passa sull'altro automa quando incontra uno stato finale della computazione seguita: se effettuo infiniti passaggi fra i due automi la stringa appartiene al linguaggio.

\newpage
Dati due automi $A_{1} = \langle Q_{1}, \Sigma, \delta_{1}, q_{0_{1}}, F_{1} \rangle$ e $A_{2} = \langle Q_{2}, \Sigma, \delta_{2}, q_{0_{2}}, F_{2} \rangle$ definisco un automa $A = \langle Q, \{a, b\}, \delta, q_{0}, F \rangle$ nel seguente modo:
\begin{itemize}
	\item $Q = Q_{1} \times Q_{2} \times \{1, 2\}$
	\item $q_{0} = (q_{0_{1}}, q_{0_{2}}, 1)$
	\item $\delta((q_{1}, q_{2}, x), a) =
		\begin{cases}
			(\delta_{1}(q_{1}, a), \delta_{2}(q_{2}, a), 2), & \mbox{se } q_{1} \in F_{1} \wedge x = 1\\
			(\delta_{1}(q_{1}, a), \delta_{2}(q_{2}, a), 1), & \mbox{se } q_{2} \in F_{2} \wedge x = 2\\
			(\delta_{1}(q_{1}, a), \delta_{2}(q_{2}, a), x), & \mbox{altrimenti}
		\end{cases}$
	\item $F = \{(q_{1}, q_{2}, 1) \in Q \mid q_{1} \in F_{1} \}$
\end{itemize}

%TODO: trascrivere esempio


\subsection{Complemento}
Abbiamo già visto che nel caso deterministico non è sempre possibile fare il complemento: il linguaggio con numero di $b$ finite non è infatti esprimibile con un automa di B\"{u}chi deterministico pur essendo il complemento del linguaggio con numero di $b$ infinite.
