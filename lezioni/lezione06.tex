\chapter{Lezione 6}
\section{Complessità di un linguaggio}
Dato un linguaggio $L \subseteq \Sigma^{*}$, è possibile definire:
\begin{itemize}
	\item $SC(L)$ (\textit{state complexity}): minimo numero di stati di un automa deterministico che accetta $L$
	\item $NSC(L)$ (\textit{non-deterministic state complexity}): minimo numero di stati di un automa non deterministico che accetta $L$
\end{itemize}

\textsf{Osservazioni}:
\begin{itemize}
	\item se un linguaggio è riconosciuto da un automa non deterministico avente $k$ stati, sicuramente sarà riconosciuto da un automa deterministico in $2^k$ stati: $SC(L) \leq 2^{NSC(L)}$ (per la costruzione vista precedentemente)
	\item ogni automa deterministico è un caso particolare di un automa non deterministico: $NSC(L) \leq SC(L)$
\end{itemize}

\textsf{Esempio}:\\
in precedenza, sono stati presi in considerazione i linguaggi:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ l'$n$-esimo simbolo da destra è una $a$}\}\]

Per quanto osservato nella sezione \ref{sect:lez4-diff}:
$SC(L_{n}) = 2^{n}$

Per quanto osservato nella sezione \ref{sect:lez5-nontodet}:
$NSC(L_{n}) = n+1$

\newpage
\section{Operazioni sui linguaggi}
\subsection{Complemento}
$SC(L^{C}) = SC({L})$: come è stato possibile osservare precedentemente, è sufficiente applicare il complemento sull'insieme degli stati finali, badando che l'automa sia completo.

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{ccc}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state] (q_1) [right=of q_0] {$q_{1}$};
				\node[state, color=red] (q_2) [below=of q_1] {$q_{2}$};
				\path[->]
					(q_0) edge [loop below] node {$a$} (q_0)
					      edge [bend left=20] node {$b$} (q_1)
					(q_1) edge [bend left=20] node {$b$} (q_0)
					      edge [color=red] node {$a$} (q_2)
					(q_2) edge [color=red, loop left] node {$a, b$} (q_2);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.05\textwidth}
			\begin{tikzpicture}
				$ \implies $
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial] (q_0) {$q_{0}$};
				\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
				\node[state, accepting, color=red] (q_2) [below=of q_1] {$q_{2}$};
				\path[->]
					(q_0) edge [loop below] node {$a$} (q_0)
					      edge [bend left=20] node {$b$} (q_1)
					(q_1) edge [bend left=20] node {$b$} (q_0)
					      edge [color=red] node {$a$} (q_2)
					(q_2) edge [color=red, loop left] node {$a, b$} (q_2);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa deterministico per $L$}&&
		\footnotesize{Automa deterministico per $L^{C}$}\\
	\end{tabular}
\end{figure}

\textsf{Attenzione}:\\
in caso di automa non completo, ricordarsi di aggiungere lo stato trappola.

\bigskip
$NSC(L^{C}) \neq NSC(L)$: questo è dovuto alla non simmetria del processo di accettazione per gli automi non deterministici.

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{c}
		\begin{tabular}{c c}
			\begin{subfigure}{0.37\textwidth}
				\begin{tikzpicture}
					\node[state, initial, accepting] (q_0) {$q_{0}$};
					\node[state] (q_1) [right=of q_0] {$q_{1}$};
					\path[->]
						(q_0) edge [loop below] node {$a$} (q_0)
						      edge [bend left=20] node {$a$} (q_1)
						(q_1) edge [bend left=20] node {$b$} (q_0);
				\end{tikzpicture}
			\end{subfigure}&
			\begin{subfigure}{0.37\textwidth}
				\begin{tikzpicture}
					\node[state, initial] (q_0) {$q_{0}$};
					\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
					\path[->]
						(q_0) edge [loop below] node {$a$} (q_0)
						      edge [bend left=20] node {$a$} (q_1)
						(q_1) edge [bend left=20] node {$b$} (q_0);
				\end{tikzpicture}
			\end{subfigure}\\
			\footnotesize{Automa per $L$}&
			\footnotesize{Automa ottenuto con la costruzione precedente}\\\\
			\begin{subfigure}{0.37\textwidth}
				\begin{tikzpicture}[->]
					\node {$q_{0}$}
						child { node [xshift=-0.5cm] {$q_{0}$}
							child { node {$q_{0}$}
								child { node [text=officegreen] {$q_{0}$} }
								child { node [text=red] {$q_{1}$} } 
							}
							child { node {$q_{1}$}
								child { node [text=red] {$\times$} }
							}
						}
						child { node [xshift=0.5cm] {$q_{1}$}
							child { node [text=red] {$\times$} }
						};
				\end{tikzpicture}
			\end{subfigure}&
			\begin{subfigure}{0.37\textwidth}
				\begin{tikzpicture}[->]
					\node {$q_{0}$}
						child { node [xshift=-0.5cm] {$q_{0}$}
							child { node {$q_{0}$}
								child { node [text=red] {$q_{0}$} }
								child { node [text=officegreen] {$q_{1}$} } 
							}
							child { node {$q_{1}$}
								child { node [text=red] {$\times$} }
							}
						}
						child { node [xshift=0.5cm] {$q_{1}$}
							child { node [text=red] {$\times$} }
						};
				\end{tikzpicture}
			\end{subfigure}\\
		\end{tabular}\\
		\footnotesize{Computazioni corrispondenti sulla stringa $aaa$}
	\end{tabular}
\end{figure}

\newpage
Come posso fare quindi? Traduco l'esempio nel suo corrispettivo automa deterministico e faccio il complemento su quello.

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state, accepting] (q_0q_1) [right=of q_0] {$q_{0, 1}$};
				\node[color=red, state] (q_2) [below=of q_0] {$q_{2}$};
				\path[->]
					(q_0)    edge [bend left=20] node {$a$} (q_0q_1)
					         edge [color=red] node {$b$} (q_2)
					(q_0q_1) edge [bend left=20] node {$b$} (q_0)
					         edge [loop below] node {$a$} (q_0q_1)
					(q_2)    edge [color=red, loop right] node {$a, b$} (q_2);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial] (q_0) {$q_{0}$};
				\node[state] (q_0q_1) [right=of q_0] {$q_{0, 1}$};
				\node[color=red, state, accepting] (q_2) [below=of q_0] {$q_{2}$};
				\path[->]
					(q_0)    edge [bend left=20] node {$a$} (q_0q_1)
					         edge [color=red] node {$b$} (q_2)
					(q_0q_1) edge [bend left=20] node {$b$} (q_0)
					         edge [loop below] node {$a$} (q_0q_1)
					(q_2)    edge [color=red, loop right] node {$a, b$} (q_2);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa deterministico}&
		\footnotesize{Automa deterministico per il complemento}\\
	\end{tabular}
\end{figure}

$NSC(L^{C}) \leq 2^{NSC(L)}$

\bigskip
Sono passato da un automa non deterministico a $n$ stati ad un automa deterministico a $2^{n}$ stati.\\
Posso migliorare questa costruzione?

Ricordiamo i due linguaggi complementari studiati nella sezione \ref{sect:lez5-compl}:\\
$L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox { $x$ contiene due simboli a distanza $n$ diversi tra loro} \}$\\
$L'_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ tutti i simboli a distanza $n$ sono uguali}\}$

Ricordiamo che $NSC(L) = 2n+2$ e $NSC(L'_{n}) \geq 2^{n}$: pur non essendo un caso peggiore mostra che posso avvicinarmi all'upper bound.\\Come per la costruzione da automa non deterministico ad automa deterministico esistono esempi in cui non si può fare meglio dell'upper bound.

\subsection{Unione}
\label{sect:lez6-union}
\subsubsection{Costruzione non deterministica}
Prima costruzione (basata sul non determinismo): scommetto su quale dei due automi riconosce la stringa
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial] (q_0) {$q_0$};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=\getnodedistance, yshift=-5pt] (A_1) [above right=of q_0] {$A_{1}$};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=\getnodedistance, yshift=5pt] (A_2) [below right=of q_0] {$A_{2}$};
		\path[->]
			(q_0) edge [bend left=25] node {$\epsilon$} (A_1.west)
			      edge [bend right=25, swap] node {$\epsilon$} (A_2.west);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa risultante dalla costruzione non deterministica}}
\end{figure}

$NSC(L_{1} \cup L_{2}) \leq NSC(L_{1}) + NSC(L_{2}) + 1$

\newpage
\subsubsection{Costruzione deterministica}
Questo tipo di costruzione è detto costruzione \textit{prodotto}.\\
Supponiamo di avere $A_{1} = \langle Q_{1}, \Sigma, \delta_{1}, q_{0_{1}}, F_{1} \rangle$ e $A_{2} = \langle Q_{2}, \Sigma, \delta_{2}, q_{0_{2}}, F_{2} \rangle$.\\
Costruisco $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ tale che:
\begin{itemize}
	\item $Q = Q_{1} \times Q_{2}$
	\item $q_{0} = (q_{0_{1}}, q_{0_{2}})$
	\item $\forall (q_{1}, q_{2}) \in Q, a \in \Sigma \ \ \ \delta((q_{1}, q_{2}), a) = (\delta_{1}(q_{1}, a), \delta_{2}(q_{2}, a))$
	\item $F$ contiene tutte le coppie con almeno uno stato finale. Formalmente:
		\[F = \{(q_{1}, q_{2}) \in Q \mid q_{1} \in F_{1} \lor q_{2} \in F_{2}\}\]
\end{itemize}
$SC(L_{1} \cup L_{2}) \leq SC(L_{1}) \cdot SC(L_{2})$

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state] (q_1) [right=of q_0] {$q_{1}$};
				\path[->]
					(q_0) edge [bend left=20] node {$a$} (q_1)
					      edge [loop below] node {$b$} (q_0)
					(q_1) edge [bend left=20] node {$a$} (q_0)
					      edge [loop below] node {$b$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (p_0) {$p_{0}$};
				\node[state] (p_1) [right=of p_0] {$p_{1}$};
				\path[->]
					(p_0) edge [bend left=20] node {$b$} (p_1)
					      edge [loop below] node {$a$} (p_0)
					(p_1) edge [bend left=20] node {$b$} (p_0)
					      edge [loop below] node {$a$} (p_1);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa per stringhe con}&
		\footnotesize{Automa per stringhe con}\\
		\footnotesize{un numero di $a$ pari}&
		\footnotesize{un numero di $b$ pari}\\
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=3cm]
		\node[state, initial, accepting] (q_0p_0) {$q_{0}, p_{0}$};
		\node[state, accepting] (q_1p_0) [right=of q_0p_0] {$q_{1}, p_{0}$};
		\node[state, accepting] (q_0p_1) [below=of q_0p_0] {$q_{0}, p_{1}$};
		\node[state] (q_1p_1) [right=of q_0p_1] {$q_{1}, p_{1}$};
		\path[->]
			(q_0p_0) edge [bend left=20] node {$a$} (q_1p_0)
			         edge [bend left=20] node {$b$} (q_0p_1)
			(q_1p_0) edge [bend left=20] node {$a$} (q_0p_0)
			         edge [bend left=20] node {$b$} (q_1p_1)
			(q_0p_1) edge [bend left=20] node {$a$} (q_1p_1)
			         edge [bend left=20] node {$b$} (q_0p_0)
			(q_1p_1) edge [bend left=20] node {$a$} (q_0p_1)
			         edge [bend left=20] node {$b$} (q_1p_0);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per stringhe con un numero di $a$ o di $b$ pari}}
\end{figure}

\newpage
\subsection{Intersezione}
\label{sect:lez6-intersect}
La costruzione prodotto va bene anche per l'intersezione, è sufficiente cambiare la definizione di stati finali:
$F = F_{1} \times F_{2}$

$SC(L_{1} \cap L_{2}) \leq SC(L_{1}) \cdot SC(L_{2})$

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state] (q_1) [right=of q_0] {$q_{1}$};
				\path[->]
					(q_0) edge [bend left=20] node {$a$} (q_1)
					      edge [loop below] node {$b$} (q_0)
					(q_1) edge [bend left=20] node {$a$} (q_0)
					      edge [loop below] node {$b$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial, accepting] (p_0) {$p_{0}$};
				\node[state] (p_1) [right=of p_0] {$p_{1}$};
				\path[->]
					(p_0) edge [bend left=20] node {$b$} (p_1)
					      edge [loop below] node {$a$} (p_0)
					(p_1) edge [bend left=20] node {$b$} (p_0)
					      edge [loop below] node {$a$} (p_1);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa per stringhe con}&
		\footnotesize{Automa per stringhe con}\\
		\footnotesize{un numero di $a$ pari}&
		\footnotesize{un numero di $b$ pari}\\
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=3cm]
		\node[state, initial, accepting] (q_0p_0) {$q_{0}, p_{0}$};
		\node[state] (q_1p_0) [right=of q_0p_0] {$q_{1}, p_{0}$};
		\node[state] (q_0p_1) [below=of q_0p_0] {$q_{0}, p_{1}$};
		\node[state] (q_1p_1) [right=of q_0p_1] {$q_{1}, p_{1}$};
		\path[->]
			(q_0p_0) edge [bend left=20] node {$a$} (q_1p_0)
			         edge [bend left=20] node {$b$} (q_0p_1)
			(q_1p_0) edge [bend left=20] node {$a$} (q_0p_0)
			         edge [bend left=20] node {$b$} (q_1p_1)
			(q_0p_1) edge [bend left=20] node {$a$} (q_1p_1)
			         edge [bend left=20] node {$b$} (q_0p_0)
			(q_1p_1) edge [bend left=20] node {$a$} (q_0p_1)
			         edge [bend left=20] node {$b$} (q_1p_0);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per stringhe con un numero di $a$ e di $b$ pari}}
\end{figure}

\bigskip
Non è invece possibile usare la costruzione non deterministica per l'intersezione: devo usare la costruzione prodotto.

$NSC(L_{1} \cap L_{2}) \leq NSC(L_{1}) \cdot NSC(L_{2})$

\newpage
\section{Esercizi a casa}
\subsection{Complemento}
Fare il complemento del seguente linguaggio:
\[L = \{ x \in \{a, b\}^{*} \mid \mbox{ il terzultimo simbolo di $x$ è una $a$}\}\]

\textsf{Soluzione}:\\
cambiamo gli stati finali all'automa presentato nella sezione \ref{sect:lez3-es2}
\begin{figure}[!ht]
	\centering
	\hspace*{-1.5cm}
	\begin{tikzpicture}[initial where=above]
		\node[state] (aaa) {\textit{aaa}};
		\node[state] (aab) [right=of aaa] {\textit{aab}};
		\node[state] (abb) [below=of aaa] {\textit{abb}};
		\node[state, accepting] (baa) [below=of aab] {\textit{baa}};
		\node[state, accepting] (bab) [below=of baa] {\textit{bab}};
		\node[state, accepting] (bba) [below=of abb] {\textit{bba}};
		\node[state] (aba) [right=of baa] {\textit{aba}};
		\node[state, initial, accepting] (bbb) [left=of abb] {\textit{bbb}};
		\path[->]
			(aaa) edge [loop above] node {$a$} (aaa)
			      edge [pos=0.25] node {$b$} (aab)
			(aab) edge [pos=0.15] node {$a$} (aba)
			      edge [swap, pos=0.10] node {$b$} (abb)
			(aba) edge [swap, pos=0.25] node {$a$} (baa)
			      edge [bend left=20, pos=0.03] node {$b$} (bab)
			(abb) edge [swap, pos=0.25] node {$a$} (bba)
			      edge [swap, pos=0.25] node {$b$} (bbb)
			(baa) edge [pos=0.25, below] node {$a$} (aaa)
			      edge [swap, pos=0.25] node {$b$} (aab)
			(bab) edge [bend left=20, pos=0.14, left] node {$a$} (aba)
			      edge [swap, pos=0.10, above] node {$b$} (abb)
			(bba) edge [swap, pos=0.10, right] node {$a$} (baa)
			      edge [swap, pos=0.25] node {$b$} (bab)
			(bbb) edge [swap, pos=0.15] node {$a$} (bba)
			      edge [loop left] node {$b$} (bbb);
	\end{tikzpicture}
\end{figure}

\newpage
\subsection{Unione}
Fare l'unione dei seguenti linguaggi:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ il numero di $a$ in $x$ è multiplo di $n$}\}\]
\[L'_{m} = \{ x \in \{a, b\}^{*} \mid \mbox{ il numero di $b$ in $x$ è multiplo di $m$}\}\]

\textsf{Soluzione}:\\
Per rendere più compatti gli automi introduciamo $s = n-1$ e $t = m-1$.
\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}[node distance=1.5cm]
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state] (q_1) [below=of q_0] {$q_{1}$};
				\node (q_i) [below=of q_1, yshift=0.3cm] {\dots};
				\node[state] (q_s) [below=of q_i, yshift=0.3cm] {$q_{s}$};
				\path[->]
					(q_0) edge node {$a$} (q_1)
					      edge [loop right] node {$b$} (q_0)
					(q_1) edge node {$a$} (q_i)
					      edge [loop right] node {$b$} (q_1)
					(q_i) edge node {$a$} (q_s)
					(q_s) edge [bend left=50] node {$a$} (q_0)
					      edge [loop right] node {$b$} (q_s);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}[node distance=1.5cm]
				\node[state, initial, accepting] (q_0) {$q_{0}$};
				\node[state] (q_1) [below=of q_0] {$q_{1}$};
				\node (q_i) [below=of q_1, yshift=0.3cm] {\dots};
				\node[state] (q_t) [below=of q_i, yshift=0.3cm] {$q_{t}$};
				\path[->]
					(q_0) edge node {$b$} (q_1)
					      edge [loop right] node {$a$} (q_0)
					(q_1) edge node {$b$} (q_i)
					      edge [loop right] node {$a$} (q_1)
					(q_i) edge node {$b$} (q_t)
					(q_t) edge [bend left=50] node {$b$} (q_0)
					      edge [loop right] node {$a$} (q_t);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa per stringhe in cui}&
		\footnotesize{Automa per stringhe in cui}\\
		\footnotesize{il numero di $a$ è multiplo di $n$}&
		\footnotesize{il numero di $b$ è multiplo di $m$}\\
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=1.8cm]
		\node[state, accepting] (q_0_0) {$q_{0,0}$};
		\node[state, accepting] (q_0_1) [right=of q_0_0] {$q_{0, 1}$};
		\node[state, accepting] (q_1_0) [below=of q_0_0] {$q_{1, 0}$};
		\node[state] (q_1_1) [below=of q_0_1] {$q_{1, 1}$};
		\node (q_0_i) [right=of q_0_1] {\dots};
		\node (q_i_0) [below=of q_1_0] {\dots};
		\node (q_1_i) [right=of q_1_1] {\dots};
		\node (q_i_1) [below=of q_1_1] {\dots};
		\node (q_i_i) [below=of q_1_i] {\dots};
		\node[state, accepting] (q_s_0) [below=of q_i_0] {$q_{s, 0}$};
		\node[state] (q_s_1) [below=of q_i_1] {$q_{s, 1}$};
		\node[state, accepting] (q_0_t) [right=of q_0_i] {$q_{0, t}$};
		\node[state] (q_1_t) [right=of q_1_i] {$q_{1, t}$};
		\node (q_s_i) [below=of q_i_i] {\dots};
		\node (q_i_t) [right=of q_i_i] {\dots};
		\node[state] (q_s_t) [below=of q_i_t] {$q_{s, t}$};
		\node (start) [above left=of q_0_0] {start};

		\node[draw, rectangle] (n_0_in) [below=of q_s_0, yshift=0.55cm] {$n_{0}$};
		\node[draw, rectangle] (n_0_out) [above=of q_0_0, yshift=-0.55cm] {$n_{0}$};
		\node[draw, rectangle] (n_1_in) [below=of q_s_1, yshift=0.55cm] {$n_{1}$};
		\node[draw, rectangle] (n_1_out) [above=of q_0_1, yshift=-0.55cm] {$n_{1}$};
		\node[draw, rectangle] (n_i_in) [below=of q_s_i, yshift=0.55cm] {$n_{i}$};
		\node[draw, rectangle] (n_i_out) [above=of q_0_i, yshift=-0.55cm] {$n_{i}$};
		\node[draw, rectangle] (n_t_in) [below=of q_s_t, yshift=0.55cm] {$n_{t}$};
		\node[draw, rectangle] (n_t_out) [above=of q_0_t, yshift=-0.55cm] {$n_{t}$};

		\node[draw, rectangle] (m_0_in) [right=of q_0_t, xshift=-0.45cm] {$m_{0}$};
		\node[draw, rectangle] (m_0_out) [left=of q_0_0, xshift=0.45cm] {$m_{0}$};
		\node[draw, rectangle] (m_1_in) [right=of q_1_t, xshift=-0.45cm] {$m_{1}$};
		\node[draw, rectangle] (m_1_out) [left=of q_1_0, xshift=0.45cm] {$m_{1}$};
		\node[draw, rectangle] (m_i_in) [right=of q_i_t, xshift=-0.45cm] {$m_{i}$};
		\node[draw, rectangle] (m_i_out) [left=of q_i_0, xshift=0.45cm] {$m_{i}$};
		\node[draw, rectangle] (m_s_in) [right=of q_s_t, xshift=-0.45cm] {$m_{s}$};
		\node[draw, rectangle] (m_s_out) [left=of q_s_0, xshift=0.45cm] {$m_{s}$};
		\path[->]
			(q_0_0)   edge [swap] node {$a$} (q_1_0)
			          edge node {$b$} (q_0_1)
			(q_0_1)   edge [swap] node {$a$} (q_1_1)
			          edge node {$b$} (q_0_i)
			(q_1_0)   edge [swap] node {$a$} (q_i_0)
			          edge node {$b$} (q_1_1)
			(q_1_1)   edge [swap] node {$a$} (q_i_1)
			          edge node {$b$} (q_1_i)
			(q_i_0)   edge [swap] node {$a$} (q_s_0)
			          edge node {$b$} (q_i_1)
			(q_i_1)   edge [swap] node {$a$} (q_s_1)
			          edge node {$b$} (q_i_i)
			(q_0_i)   edge [swap] node {$a$} (q_1_i)
			          edge node {$b$} (q_0_t)
			(q_1_i)   edge [swap, pos=0.62] node {$a$} (q_i_i)
			          edge node {$b$} (q_1_t)
			(q_i_i)   edge [swap, pos=0.38] node {$a$} (q_s_i)
			          edge node {$b$} (q_i_t)
			(q_s_0)   edge [swap] node {$a$} (n_0_in)
			          edge node {$b$} (q_s_1)
			(q_s_1)   edge [swap] node {$a$} (n_1_in)
			          edge node {$b$} (q_s_i)
			(q_s_i)   edge [swap, pos=0.7] node {$a$} (n_i_in)
			          edge node {$b$} (q_s_t)
			(q_0_t)   edge [swap] node {$a$} (q_1_t)
			          edge node {$b$} (m_0_in)
			(q_1_t)   edge [swap] node {$a$} (q_i_t)
			          edge node {$b$} (m_1_in)
			(q_i_t)   edge [swap] node {$a$} (q_s_t)
			          edge [pos=0.6] node {$b$} (m_i_in)
			(q_s_t)   edge [swap] node {$a$} (n_t_in)
			          edge node {$b$} (m_s_in)

			(n_0_out) edge node {} (q_0_0)
			(n_1_out) edge node {} (q_0_1)
			(n_i_out) edge node {} (q_0_i)
			(n_t_out) edge node {} (q_0_t)
			(m_0_out) edge node {} (q_0_0)
			(m_1_out) edge node {} (q_1_0)
			(m_i_out) edge node {} (q_i_0)
			(m_s_out) edge node {} (q_s_0)

			(start)   edge node {} (q_0_0);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per stringhe in cui il numero di $a$ è multiplo di $n$ o il numero di $b$ è multiplo di $m$}}
\end{figure}
