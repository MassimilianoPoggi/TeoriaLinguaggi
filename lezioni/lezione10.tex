\chapter{Lezione 10}
\section{Automi bidirezionali}
\subsection{Definizione}
Un automa bidirezionale non deterministico $M = \langle Q, \Sigma, \delta, q_{0}, q_{F} \rangle$ è una quintupla dove viene ridefinita la funzione di transizione:
\[ \delta : Q \times (\Sigma \cup \{\triangleright, \triangleleft \}) \to 2^{Q \times \{-1, 0, 1\}}\]

dove -1 indica lo spostarsi verso sinistra, 0 stare fermi nella cella e 1 spostarsi verso destra, con alcune restrizioni:
\begin{itemize}
	\item non posso spostarmi a sinistra col simbolo $\triangleright$
	\item non posso spostarmi a destra col simbolo $\triangleleft$ a meno di non andare nello stato finale
\end{itemize}

$M$ è detto deterministico se ad ogni mossa associa una sola scelta:
\[ \forall q \in Q, \sigma \in (\Sigma \cup \{\triangleright, \triangleleft\}) \ \ \#\delta(q, \sigma) \leq 1\]

\begin{figure}[ht!]
	\centering
	\begin{tikzpicture}[shorten >=0pt]
		% override global `shorten >=` value to remove tiny gap in the top-right corner
		\draw[step=0.4cm,gray,very thin] (0,0) grid (6,0.4);

		\draw (0, -0.3) -- (0.5, -0.3);
		\draw (0.5, -0.3) arc(90:-90:0.1);

		\draw (0.5, -0.5) -- (0.3, -0.5);
		\draw (0.3, -0.5) arc(90:270:0.1);

		\draw (0.3, -0.7) -- (1.3, -0.7);
		\draw (1.3, -0.7) arc(90:-90:0.1);

		\draw (1.3, -0.9) -- (0.7, -0.9);
		\draw (0.7, -0.9) arc(90:270:0.1);

		\draw (0.7, -1.1) -- (3.8, -1.1);
		\draw (3.8, -1.1) arc(90:-90:0.1);

		\draw (3.8, -1.3) -- (0.3, -1.3);
		\draw (0.3, -1.3) arc(90:270:0.1);

		\draw (0.3, -1.5) -- (2.2, -1.5);
		\draw (2.2, -1.5) arc(90:-90:0.1);

		\draw (2.2, -1.7) -- (1.9, -1.7);
		\draw (1.9, -1.7) arc(90:270:0.1);

		\draw (1.9, -1.9) -- (4.1, -1.9);
		\draw (4.1, -1.9) arc(90:-90:0.1);

		\draw (4.1, -2.1) -- (3.1, -2.1);
		\draw (3.1, -2.1) arc(90:270:0.1);

		\draw (3.1, -2.3) -- (4.5, -2.3);
		\draw (4.5, -2.3) arc(90:-90:0.1);

		\draw (4.5, -2.5) -- (1.4, -2.5);
		\draw (1.4, -2.5) arc(90:270:0.1);

		\draw (1.4, -2.7) -- (3.1, -2.7);
		\draw (3.1, -2.7) arc(90:-90:0.1);

		\draw (3.1, -2.9) -- (2.3, -2.9);
		\draw (2.3, -2.9) arc(90:270:0.1);

		\draw (2.3, -3.1) -- (6.5, -3.1);

		\node at (-0.2, 0.2) {$\triangleright$};
		\node at (6.2, 0.2) {$\triangleleft$};
		\node at (-0.3, -0.3) {$q_{0}$};
		\node at (6.8, -3.1) {$q_{1}$};
	\end{tikzpicture}
\end{figure}

La computazione accetta quando l'automa supera il simbolo $\triangleleft$ andando nello stato finale.
La computazione può non terminare.

\subsection{Conversione in automi monodirezionali}
Ogni automa monodirezionale può essere banalmente convertito in automa bidirezionale non utilizzando mai le transizioni stazionarie o all'indietro.
Dimostriamo ora che gli automi bidirezionali riconoscono la classe dei linguaggi regolari costruendo un automa monodirezionale equivalente.

\bigskip
\textsf{Dimostrazione}:\\
consideriamo una porzione del nastro di input contenente una stringa $w$.
Supponiamo di avere un automa monodirezionale che inizia ad elaborare $w$ da uno stato $p$: esce dalla sezione di $w$ nello stato $\delta(p, w)$.
Consideriamo ora un automa bidirezionale nella stessa situazione: ho diverse possibilità
\begin{itemize}
	\item esco a destra
	\item esco a sinistra
	\item non esco
		\begin{itemize}
			\item perché la funzione non è definita
			\item perché entro in un loop
		\end{itemize}
\end{itemize}
Posso però entrare anche da destra con le stesse considerazioni.

\subsubsection{Tabelle di transizione}
Introduciamo una struttura detta \textit{tabella di transizione}:
\[ \tau: Q \to (Q \cup \{ \bot \}) \]
E il loro insieme:
\[ T = ( Q \cup \{ \bot \} ) ^ {Q} \]
Data una stringa $w \in \Sigma^{*}$ voglio associare una tabella di transizione a $\triangleright w$:
\begin{itemize}
	\item non posso uscire da sinistra
	\item $\tau_{w}(p) = q \iff \exists$ cammino di computazione su una parte di nastro che contiene $\triangleright w$ che inizia nello stato $p$ sul simbolo più a destra e dopo un numero di passi lascia la parte di nastro a destra nello stato $q$
\end{itemize}

Definiamo la tabella di transizione induttivamente:
\begin{itemize}
	\item $\tau_{\epsilon}(p) = q \iff \delta(p, \triangleright) = (q, +1)$
	\item $\forall x \in \Sigma^{*}, a \in \Sigma \ \ \tau_{xa}(p) =
		\begin{cases}
			q, & \mbox{se } \delta(p, a) = (q, +1)\\
			q, & \mbox{se } \exists p_{1}, \dots, p_{k}, q_{1}, \dots, q_{k} \in Q\\&\ \ \delta(p, a) = (q_{1}, -1),\\&\ \ \forall i \in [1, k] \ \ \tau_{x}(q_{i}) = p_{i},\\&\ \ \forall i \in [2, k] \ \ \delta(p_{i-1}, a) = (q_{i}, -1),\\&\ \  \delta(p_{k}, a) = (q, +1)
		\end{cases}$
\end{itemize}

\textsf{Osservazioni}:
\begin{itemize}
	\item $\tau_{x} = \tau_{x_{1}} \Rightarrow \tau_{xa} = \tau_{x_{1}a}$
	\item il numero di tabelle è finito ($(\#Q + 1)^{\#Q})$
	\item ho una funzione $f_{a}(\tau)$ che data una tabella $\tau_{w}$ calcola la tabella $\tau_{wa}$
\end{itemize}

\bigskip
Definisco un automa monodirezionale $M' = \langle Q', \Sigma, \delta', q_{0}', F' \rangle$ nel seguente modo:
\begin{itemize}
	\item $Q' = Q \times T$
	\item $q_{0}' = (q_{0}, \tau_{\epsilon})$
	\item $\delta'((p, \tau), a) = (q, \tau')$ dove:
		\begin{itemize}
			\item $p$ è lo stato in cui arrivo nella cella di $a$ per la prima volta
			\item $\tau$ è la tabella di transizione associata alla parte di parola che si trova a sinistra
		\end{itemize}
		$\ \ \ \ \tau' = f_{a}(\tau), \ q = \tau'(p)$
	\item $F = \{ (p, \tau) \mid f_{\triangleleft}(\tau)(p) = q_{f} \}$
\end{itemize}

\subsection{Problema di Sakoda-Sipser}
Si può fornire una trasformazione polinomiale nel numero di stati da un automa monodirezionale non deterministico a bidirezionale deterministico e da bidirezionale non deterministico a bidirezionale deterministico?

Il problema è ancora aperto: si sa solo che gli upper bound sono esponenziali.

\section{Pumping lemma per i linguaggi regolari}
Dato $L$ regolare esiste una costante $n$ tale che $\forall w \in L : |w| \geq n$ posso scrivere $w = uvz$:
\begin{itemize}
	\item $|uv| \leq n$
	\item $v \neq \epsilon$
	\item $\forall k \in \mathbb{N} \ \ uv^{k}z \in L$
\end{itemize}

%TODO: disegnino automa

\textsf{Esempio}:\\
dimostriamo che $L = \{ a^{p} \mid p \mbox{ è primo}\}$ non è regolare.\\
Supponiamo per assurdo che $L$ sia regolare, e consideriamo una stringa $w = a^{p}$ con $p \mbox{ primo} \geq n$: $w = a^{p}, u = a^{i}, v = a^{j}, z = a^{h}$.
Consideriamo ora le stringhe della forma $uv^{k}z = a^{i}a^{jk}a^{h} = a^{i+h}a^{jk}$ che per il pumping lemma devono appartenere a $L$.
Sapendo che $i+j+k = p$ so anche che $i + h + jk = p + j(k-1)$: per $k = p+1$ ottengo che $p(j+1)$ deve essere un numero primo: assurdo $\Rightarrow L$ non è regolare.
