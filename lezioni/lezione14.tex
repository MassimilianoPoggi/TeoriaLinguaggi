\chapter{Lezione 14}
\section{Rappresentazione logica dei linguaggi}
Si consideri il seguente automa:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial] (q_0) {$q_{0}$};
		\node[state, accepting] (q_2) [below right=of q_0] {$q_{2}$};
		\node[state] (q_1) [above right=of q_2] {$q_{1}$};
		\path[->]
			(q_0) edge [loop above] node {$c$} (q_0)
			      edge node {$b$} (q_1)
			      edge [bend left=20] node {$a$} (q_2) 
			(q_1) edge node {$a$} (q_2)
			(q_2) edge [loop below] node {$a$} (q_2)
			      edge [bend left=20] node {$c$} (q_0);
	\end{tikzpicture}
\end{figure}

Il linguaggio descritto da quest'automa rispetta tre proprietà:
\begin{itemize}
	\item le parole del linguaggio finiscono con una $a$. Formalmente:
		\[\forall x \ (\exists y \ S(x,y)) \implies a(x)\]
	\item dopo ogni $a$ non c'è una $b$. Formalmente:
		\[\forall x \ a(x) \implies (\forall y \ S(x, y) \implies \neg b(y)) \]
	\item dopo ogni $b$ c'è una $a$. Formalmente:
		\[\forall x \ b(x) \implies (\exists y \ S(x, y) \wedge a(y)) \]	
\end{itemize}
dove le variabili sono posizioni all'interno della parola, $S(x, y)$ indica che la posizione $y$ è successore della posizione $x$ e $a(x)$ indica che nella posizione $x$ della parola c'è una $a$.

\newpage
\section{Logica del prim'ordine}
La logica del prim'ordine è costituita dai seguenti elementi:
\begin{itemize}
	\item un insieme infinito di nomi di variabili
	\item simboli logici:
		\begin{itemize}
			\item connettori: $\neg$ (not), $\wedge$ (and), $\lor$ (or), $\implies$ (implica),\\$\iff$ (equivalente)
			\item quantificatori: $\forall$ (per ogni), $\exists$ (esiste)
			\item l'operatore di uguaglianza fra posizioni $=$
		\end{itemize}
	\item un vocabolario (o segnatura):
		\begin{itemize}
			\item l'operatore di successore $S(x, y)$
			\item le relazioni d'ordine $<, >, \leq, \geq$
			\item predicati sulle lettere nelle posizioni come $a(x)$
		\end{itemize}
	\item formule logiche definite per induzione:
		\begin{itemize}
			\item formule atomiche che derivano direttamente dai simboli definiti nel vocabolario:
				\[\alpha, \beta := S(x, y) \ | \  a(x) \ | \ x < y\]
			\item formule definite induttivamente grazie ai connettori:
				\[\varphi, \psi := \varphi \lor \psi \ | \ \neg \varphi \ | \ \exists x \ \varphi \]
		\end{itemize}
\end{itemize}

\bigskip
\textsf{Osservazioni}:
\begin{itemize}
	\item la definizione induttiva precedente per le formule è sufficiente: gli altri connettori sono infatti ricavabili utilizzando solo i tre definiti.
		\begin{itemize}
			\item $\varphi \implies \psi \equiv \neg \varphi \wedge \psi$
			\item $\forall x \ \varphi \equiv \neg \exists x \ \neg \varphi$
			\item $\varphi \wedge \psi \equiv \neg (\neg \varphi \lor \neg \psi)$
		\end{itemize}
	\item l'operatore di successore è meno potente degli operatori d'ordine: non è possibile infatti esprimere l'operatore d'ordine con una formula finita con solo il successore, mentre è possibile il viceversa. Infatti:
		\[S(x, y) \equiv y > x \wedge (\neg \exists z \ x < z \wedge z < y) \]
\end{itemize}

\section{Logica monadica del secondo ordine}
Alcuni linguaggi non sono però esprimibili con la logica del prim'ordine: si prenda come esempio il linguaggio delle parole con un numero pari di $a$.

La logica monadica del secondo ordine permette di avere come variabili non solo posizioni, ma anche insiemi di posizioni, e aggiunge al vocabolario l'operatore di appartenenza ad un insieme $x \in X$.

In questa logica è possibile descrivere il linguaggio presentato sopra: l'idea è di definire un insieme che contiene una $a$ sì e una no: le stringhe con un numero di $a$ pari avranno solo una delle due fra la prima e l'ultima $a$ che appartengono a quest'insieme.

Caratterizziamo quindi l'insieme $X$ che svolge questo compito. $\exists X \ \forall x$:
\begin{itemize}
	\item ogni elemento di $X$ è una $a$:
		\[x \in X \implies a(x) \]
	\item se $x \in X$ la $a$ successiva $y \notin X$:
		\[x \in X \Rightarrow (\forall y \ (x < y \wedge a(y) \wedge (\nexists z \ x < z \wedge z < y \wedge a(z)) \Rightarrow y \notin X))  \]
	\item se $x$ è una $a$ e non è in $X$ allora la $a$ successiva è in $X$:
		\[a(x) \wedge x \notin X \Rightarrow (\forall y \ (x < y \wedge a(y) \wedge (\nexists z \ x < z \wedge z < y \wedge a(z)) \Rightarrow y \in X))  \]
\end{itemize}
Inoltre devono valere le seguenti due proprietà:
\begin{itemize}
	\item la prima $a$ appartiene all'insieme $X$:
		\[\forall x \ (a(x) \wedge (\nexists y \ y < x \wedge a(y)) \implies x \in X\]
	\item l'ultima $a$ non appartiene all'insieme $X$:
		\[\forall x \ (a(x) \wedge (\nexists y \ x < y \wedge a(y)) \implies x \notin X\]
\end{itemize}

\bigskip
\textsf{Osservazione}:\\
al contrario della logica del prim'ordine l'operatore di successore e gli operatori d'ordine hanno la stessa potenza. \`{E}  infatti possibile esprimere gli operatori d'ordine mediante solo l'operatore di successore costruendo induttivamente l'insieme costituito da $x$ e da tutti i successori degli elementi nell'insieme:
\[x < y \equiv (\forall Y \ (x \in Y \wedge (\forall u, z \ S(u, z) \Rightarrow (u \in Y \Rightarrow z \in Y))) \Rightarrow y \in Y) \wedge y \neq x \]