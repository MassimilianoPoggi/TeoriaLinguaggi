\chapter{Lezione 4}
\section{Distinguibilità tra stringhe}
\label{sect:lez4-diff}
Dato un linguaggio $L \subseteq \Sigma^{*}$, due stringhe $x, y \in \Sigma^{*}$ si dicono \textit{distinguibili} rispetto a $L \iff \exists z \in \Sigma^{*} : (xz \in L \wedge yz \notin L) \lor (xz \notin L \wedge yz \in L)$.

\bigskip
\textsf{Teorema}:\\
dati un linguaggio $L \subseteq \Sigma^{*}$ e un insieme di parole $X \subseteq \Sigma^{*}$ tali che tutte le stringhe in $X$ siano distinguibili fra loro allora ogni automa deterministico che accetta $L$ ha almeno $\#X$ stati.

\bigskip
\textsf{Dimostrazione}:\\
si supponga che $X = \{x_{1}, ..., x_{k}\}$ e, per assurdo, che $A$ sia un automa deterministico per $L$ con meno di $k$ stati.\\
Si faccia quindi girare l'automa su tutte le stringhe: dal momento che ci sono più stringhe che stati $\exists i, j \in [1, k] \ \ \ i \neq j : \delta(q_{0}, x_{i}) = \delta(q_{0}, x_{j})$.\\
Pertanto, $\forall z \in \Sigma^{*} \ \delta(q_{0}, x_{i}z) = \delta(q_{0}, x_{j}z)$: ne segue quindi che $x_{i}$ non è distinguibile da $x_{j}$ (assurdo).

\bigskip
\textsf{Esempio}:\\
è possibile utilizzare questo teorema per dimostrare che l'automa descritto nella sezione \ref{sect:lez3-es2} è minimale.

Tuttavia, in questo caso, si provi a compiere un ulteriore sforzo e si dimostri un caso più generale, ovvero:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{ l'$n$-esimo simbolo da destra è una } a\}\]

Per adempiere allo scopo preposto, si prenda in considerazione l'insieme $X_{n} = \{a, b\}^{n}$ e si dimostri che due stringhe qualsiasi di $X_{n}$ sono distinguibili fra loro. Le due stringhe in questione sono $\sigma = \sigma_{1}\sigma_{2}\dots\sigma_{n}$ e $\gamma = \gamma_{1}\gamma_{2}\dots\gamma_{n}$, con $\sigma \neq \gamma$: poiché le due stringhe sono diverse, $\exists i \in [1, n] \ \sigma_{i} \neq \gamma_{i}$.

Le due stringhe sono distinguibili $\forall z \in \{a, b\}^{i-1}$, in quanto queste stringhe portano il carattere diverso all'$n$-esimo posto da destra.

Si può quindi concludere che il numero minimo di stati necessari per gli automi deterministici che riconoscono $L_{n}$ è $\#X = 2^{n}$.

\newpage
\section{Automi non deterministici}
\label{sect:lez4-nondet}
Un automa non deterministico è una quintupla analoga ad un automa deterministico, ma che modifica la definizione di funzione di transizione:
\[\delta: Q \times \Sigma \to 2^{Q}\]
\textsf{Esempio}:\\
automa che riconosce le stringhe con $a$ come terzultimo simbolo

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c @{\hskip 2cm} c}
		\begin{tikzpicture}
			\node[state,initial] (q_0)	 {$q_{0}$}; 
			\node[state] (q_1) [below=of q_0] {$q_{1}$}; 
			\node[state] (q_2) [below=of q_1] {$q_{2}$};
			\node[state, accepting] (q_3) [below=of q_2] {$q_{3}$};
			\path[->]
				(q_0) edge [loop right] node {$a, b$} (q_0)
				      edge node {$a$} (q_1)
				(q_1) edge node {$a, b$} (q_2)
				(q_2) edge node {$a, b$} (q_3);
		\end{tikzpicture}&
		\begin{tikzpicture}[->]
			\node {$q_{0}$}
				child { node [xshift=-0.5cm] {$q_{0}$} 
					child { node {$q_{0}$} 
						child { node {$q_{0}$}
							child { node [text=red] {$q_{0}$} }
						}
					}
					child { node {$q_{1}$}
						child { node {$q_{2}$}
							child { node [text=officegreen] {$q_{3}$} }
						}
					}
				}
				child { node [xshift=0.5cm] {$q_{1}$}
					child { node {$q_{2}$} 
						child { node {$q_{3}$}
							child { node [text=red] {$\times$} }
						}
					}
				};

			\node at (2.5, -0.75) {$a$};
			\node at (2.5, -2.25) {$a$};
			\node at (2.5, -3.75) {$b$};
			\node at (2.5, -5.25) {$b$};
		\end{tikzpicture}\\
		\footnotesize{Automa non deterministico} &
		\footnotesize{Computazione sulla stringa $aabb$}\\
	\end{tabular}
\end{figure}

Una stringa è accettata se esiste un cammino accettante. Formalmente:
\[x \in L \iff \delta(q_{0}, x) \cap F \neq \emptyset\]

% subsection title with bold epilon and table of contents title with normal epsilon
\subsection[Automi con \texorpdfstring{$\epsilon$}{e}-mosse]{Automi con $\bm{\epsilon}$-mosse}
Una $\epsilon$-mossa è una transizione che l'automa può compiere senza leggere alcun carattere di input.\medskip\\
\textsf{Esempio}:\\
in corrispondenza del linguaggio $L = \{ a^{*}b^{*}a^{*} \}$:

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (q_0) {$q_{0}$}; 
		\node[state] (q_1) [right=of q_0] {$q_{1}$}; 
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge [loop below] node {$a$} (q_0)
			      edge node {$\epsilon$} (q_1)
			(q_1) edge [loop below] node {$b$} (q_1)
			      edge node {$\epsilon$} (q_2)
			(q_2) edge [loop below] node {$a$} (q_2);
	\end{tikzpicture}\\
	\caption*{\footnotesize{Automa con $\epsilon$-mosse}}
\end{figure}

\newpage
\section{Corrispondenza con grammatiche regolari}
Data una grammatica $G = \langle V, \Sigma, P, S \rangle$ di tipo 3, è possibile costruire un automa non deterministico $A = \langle Q, \Sigma, \delta, q_{0}, F\rangle$ equivalente fatto in questo modo:
\begin{itemize}
	\item $Q = V \cup \{{q_{f}}\}$
	\item $q_{0} = S$
	\item $\delta$ così costruita:
		\begin{itemize}
			\item $\forall (A \to aB) \in P \ B \in \delta(A, a)$
			\item $\forall (A \to a) \in P \ q_{f} \in \delta(A, a)$
		\end{itemize}
	\item $F = \{q_{f}\}$ 
\end{itemize}

Viceversa, posso trasformare l'automa $A$ in una grammatica $G$:
\begin{itemize}
	\item $V = Q$
	\item $S = q_{0}$
	\item $\forall A \in Q, a \in \Sigma, B \in \delta(A, a) \ (A \to aB) \in P$\\e inoltre $\delta(A, a) \cap F \neq \emptyset \Rightarrow (A \to a) \in P$
\end{itemize}

\textsf{Esempio}:\\
linguaggio composto da stringhe della forma $(ab)^{*}ab$

\begin{center}
	\begin{prodlist}[Grammatica]{3}
		\production{S}{aA}
		\production{A}{bS}
		\production{A}{b}
	\end{prodlist}
\end{center}

Si provi a svolgere una derivazione:\\
$\underline{S} \xRightarrow[(1)]{} a\underline{A} \xRightarrow[(2)]{} ab\underline{S} \xRightarrow[(1)]{} aba\underline{A} \xRightarrow[(3)]{} \boldsymbol{abab}$

\bigskip
Costruendo il corrispondente automa:

\begin{center}
	\begin{figure}[!ht]
		\centering
		\begin{tikzpicture}
			\node[state,initial] (S) {$S$}; 
			\node[state] (A) [right=of S] {$A$}; 
			\node[state, accepting] (q_f) [right=of A] {$q_{f}$};
			\path[->]
				(S) edge [bend left = 20] node {$a$} (A)
				(A) edge [bend left = 20] node {$b$} (S)
				    edge node {$b$} (q_f);
		\end{tikzpicture}
		\caption*{\footnotesize{Automa}}
	\end{figure}
\end{center}

\vspace{-8mm}
$\underline{S} \xRightarrow[(a)]{} \underline{A} \xRightarrow[(b)]{} \underline{S} \xRightarrow[(a)]{} \underline{A} \xRightarrow[(b)]{} \boldsymbol{q_f}$

\bigskip
\textsf{Osservazioni}:
\begin{itemize}
\item l'automa non fa altro che simulare la derivazione direttamente
\item la memoria dell'automa coincide con la variabile da derivare
\end{itemize}

\newpage
\section{Esercizi a casa}
\subsection{Lower bound}
\label{sect:lez4-es1}
Dato il seguente linguaggio:
\[L = \{ x \in \{a, b\}^{*} \mid \mbox{ il penultimo e il terzultimo simbolo di $x$ sono uguali}\}\]

Dimostrare che:
\begin{itemize}
	\item tutte le stringhe di lunghezza 3 sono distinguibili tra loro per $L$
	\item $\epsilon$ è distinguibile da ogni stringa di lunghezza 3
\end{itemize}

Operativamente, occorre trovare un lower bound sul numero di stati degli automi che riconoscono $L$.

Quindi, si provi a costruire un automa deterministico con numero di stati pari al lower bound.

Si riesce infine a fare di meglio con un automa non deterministico?

\bigskip
\textsf{Soluzione}:

\begin{itemize}
	\item Si distinguano tre casi diversi la cui unione è uguale a $\{a, b\}^{3} \times \{a, b\}^{3}$:
		\begin{itemize}
			\item $\gamma, \sigma \in \{a, b\}^{3}$ con $\gamma_{3} \neq \sigma_{3}$: la stringa $aa$ li distingue
			\item $\gamma, \sigma \in \{a, b\}^{3}$ con $\gamma_{2} \neq \sigma_{2} \wedge \gamma_{3} = \sigma_{3}$: la stringa $a$ li distingue
			\item $\gamma, \sigma \in \{a, b\}^{3}$ con $\gamma_{1} \neq \sigma_{1} \wedge \gamma_{2} = \sigma_{3}$: la stringa $\epsilon$ li distingue
		\end{itemize}
	\item $\forall \gamma \in \{a, b\}^{3} \ \exists z = \gamma_{3}a : \gamma z \in L, \epsilon z = z \notin L \Rightarrow \epsilon$ distinguibile da $\gamma$
\end{itemize}

Si prendano in considerazione ora le stringhe di lunghezza
\begin{itemize}
	\item $n = 1$: $a$ indistinguibile da $aba$, $b$ indistinguibile da $bab$
	\item $n = 2$: $\forall \sigma \in \{a, b\}^{2} \ \sigma$ indistinguibile da
		$\gamma =
			\begin{cases}
				a \sigma, \mbox{se } \sigma_{1} = b\\
				b \sigma, \mbox{se } \sigma_{1} = a\\
			\end{cases}$\\
		Si noti che questo risultato è ovvio se si considera quello del punto precedente: essendo $a$ indistinguibile da $aba$, ne segue che $aa$ e $ab$ sono indistinguibili da $baa$ e $bab$ (ottenuti applicando le transizioni opportune allo stato $aba$); discorso analogo con $b$ e le stringhe $ba$ e $bb$
	\item $n > 3$: $\forall n \in [4, +\infty), \sigma \in \{a, b\}^{n} \ \sigma$ indistinguibile da $\gamma = \sigma_{n-2}\sigma_{n-1}\sigma_{n}$
\end{itemize}
\`E stato individuato $X = \{a, b\}^{3} \cup \{\epsilon\}$.\\
Dal momento che $\#X = 9$, questo costituisce un lower bound massimale per $L$.

\newpage
Per ottenere l'automa corrispondente, occorre modificare leggermente quello visto nella sezione \ref{sect:lez3-es2}, cambiando gli stati accettanti e aggiungendo lo stato $\epsilon$. Le transizioni da $\epsilon$ emergono dal discorso sulla distinguibilità fatto precedentemente per le stringhe $a$ e $b$:

\begin{figure}[!ht]
	\centering
	\hspace*{-1.5cm}
	\begin{tikzpicture}[initial where=right]
		\node[state,accepting] (aaa) {\textit{aaa}};
		\node[state,accepting] (aab) [right=of aaa] {\textit{aab}};
		\node[state] (abb) [below=of aaa] {\textit{abb}};
		\node[state] (baa) [below=of aab] {\textit{baa}};
		\node[state] (bab) [below=of baa] {\textit{bab}};
		\node[state, accepting] (bba) [below=of abb] {\textit{bba}};
		\node[state] (aba) [right=of baa] {\textit{aba}};
		\node[state, accepting] (bbb) [left=of abb] {\textit{bbb}};
		\node[state, initial] (epsilon) [below=of aba, xshift=\getnodedistance] {$\epsilon$};
		\path[->]
			(aaa)     edge [loop above] node {$a$} (aaa)
			          edge [pos=0.25] node {$b$} (aab)
			(aab)     edge [pos=0.15] node {$a$} (aba)
			          edge [swap, pos=0.10] node {$b$} (abb)
			(aba)     edge [swap, pos=0.25] node {$a$} (baa)
			          edge [bend left=20, pos=0.03] node {$b$} (bab)
			(abb)     edge [swap, pos=0.25] node {$a$} (bba)
			          edge [swap, pos=0.25] node {$b$} (bbb)
			(baa)     edge [pos=0.25, below] node {$a$} (aaa)
			          edge [swap, pos=0.25] node {$b$} (aab)
			(bab)     edge [bend left=20, pos=0.14, left] node {$a$} (aba)
			          edge [swap, pos=0.10, above] node {$b$} (abb)
			(bba)     edge [swap, pos=0.10, right] node {$a$} (baa)
			          edge [swap, pos=0.25] node {$b$} (bab)
			(bbb)     edge [swap, pos=0.15] node {$a$} (bba)
			          edge [loop left] node {$b$} (bbb)
			(epsilon) edge [swap, pos=0.15] node {$a$} (aba)
			          edge [pos=0.15] node {$b$} (bab);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico per $L$}}
\end{figure}

Allo stesso modo, per ottenere un automa non deterministico, è necessario modificare quello visto nella sezione \ref{sect:lez4-nondet}:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (q_0)	 {$q_{0}$};
		\node[state] (q_1) [below left=of q_0] {$q_{1}$};
		\node[state] (q_2) [below right=of q_1] {$q_{2}$};
		\node[state, accepting] (q_3) [below =of q_2] {$q_{3}$};
		\node[state] (q_4) [below right=of q_0] {$q_{4}$};
		\path[->]
			(q_0) edge [loop above] node {$a, b$} (q_0)
			      edge [swap] node {$a$} (q_1)
			      edge node {$b$} (q_4)
			(q_1) edge [swap] node {$a$} (q_2)
			(q_2) edge [swap] node {$a, b$} (q_3)
			(q_4) edge node {$b$} (q_2);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa non deterministico per $L$}}
\end{figure}

L'automa ottenuto ha 6 stati (considerando nel conteggio lo stato trappola): 3 in meno di quelli richiesti come minimo da un automa deterministico.

\newpage
\subsection{Assenza di lower bound}
Dati i seguenti linguaggi:
\begin{itemize}
	\item $L_{1} = \{ x \in \{a, b\}^{*} \mid \charcount{a}{x} = \charcount{b}{x} \}$
	\item $L_{2} = \{ a^{n}b^{n} \mid n \in \mathbb{N} \}$
	\item $L_{3} = \{ ww^{R} \mid w \in \{a, b\}^{*}\}$ definendo $w^{R}$ come $w$ rovesciata\\(eg: $(aab)^{R} = baa$)
\end{itemize}
Dimostrare che non è possibile riconoscere questi linguaggi con un automa deterministico.

Suggerimento: dimostrare che la cardinalità degli insiemi $X$ è infinita.\bigskip

\textsf{Soluzione}:

\begin{itemize}
	\item $L_{1}$: si consideri la relazione di equivalenza $R$ così definita
		\[R(x_{1}, x_{2}) \iff \charcount{a}{x_{1}} - \charcount{b}{x_{1}} = \charcount{a}{x_{2}} - \charcount{b}{x_{2}}\]
		Indicando con $R_{n} = \{ x \in \Sigma^{*} \mid \charcount{a}{x} - \charcount{b}{x} = n \}$ per $n \in \mathbb{Z}$ le classi di equivalenza individuate, si prenda in considerazione $\Sigma^{*}/R$.\\
		$\forall i, j \in \mathbb{Z}, x_{i} \in R_{i}, x_{j} \in R_{j} \ i \neq j \Rightarrow x_{i}$ è distinguibile da $x_{j}$: è sufficiente costruire la stringa
		$z =
			\begin{cases}
				b^{i}, \mbox{se } i \geq 0\\
				a^{i}, \mbox{se } i < 0\\
			\end{cases}$
		per cui $x_{i}z \in L_{1} \wedge x_{j}z \notin L_{1}$.\\
		Poiché $\#(\Sigma^{*}/R) = + \infty$, è possibile concludere che $L_{1}$ non è riconoscibile da un automa a stati finiti.\\
		Si può anche fare un ragionamento più semplice analogo a quello fatto per il prossimo caso.
	\item $L_{2}$: si costruisca l'insieme $X_{2} = \{ a^{n} \mid n \in \mathbb{N} \}$.\\
		Indicando con $x_{k}$ l'elemento di $X$ contenente k $a$, si prendano in considerazione due stringhe $x_{i}, x_{j}$ con $i \neq j$.
		Le due stringhe sono distinguibili: è sufficiente infatti considerare la stringa $z = b^{i}$, per cui $x_{i}z \in L_{2} \wedge x_{j}z \notin L_{2}$.\\
		$X_{2}$ è pertanto formato da stringhe distinguibili fra loro e $\#X_{2} = +\infty$.
	\item $L_{3}$: si prenda in considerazione ora l'insieme $X_{3} = \{ a^{n}b \mid n \in \mathbb{N} \}$.\\
		$\forall i, j \in \mathbb{N}, i \neq j \Rightarrow x_{i}$ è distinguibile da $x_{j}$: basta infatti considerare la stringa $z = x_{i}^{R}$ per cui $x_{i}z \in L_{3} \wedge x_{j}z \notin L_{3}$.\\
		Le stringhe in $X_{3}$ sono pertanto distinguibili fra loro e $\#X_{3} = +\infty$.
\end{itemize}
