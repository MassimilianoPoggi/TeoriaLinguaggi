\chapter{Lezione 9}
\section{Minimalizzazione di automi}
\subsection{Esempio}
Consideriamo l'algoritmo visto nella sezione \ref{sect:lez8-minim} e forniamo ora un esempio.

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (A) {$A$};
		\node[state] (B) [right=of A] {$B$};
		\node[state, accepting] (C) [right=of B] {$C$};
		\node[state, accepting] (D) [right=of C] {$D$};
		\path[->]
			(A) edge [loop above] node {$b$} (A)
			    edge [bend left=20] node {$a$} (B)
			(B) edge [bend left=20] node {$a$} (A)
			    edge node {$b$} (C)
			(C) edge [bend left] node {$a, b$} (D)
			(D) edge [bend left] node {$a$} (C)
			    edge [loop right] node {$b$} (D);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico iniziale}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\draw[shorten >=0pt, step=1cm] (0,0) grid (1, 3);
		\draw[shorten >=0pt, step=1cm] (1, 0) grid (2, 2);
		\draw[shorten >=0pt, step=1cm] (2, 0) grid (3, 1);

		\node[color=red] at (0.5, 0.5) {$X$};
		\node[color=red] at (1.5, 0.5) {$X$};
		\node[color=red] at (0.5, 1.5) {$X$};
		\node[color=red] at (1.5, 1.5) {$X$};
		\node[color=blue] at (0.5, 2.5) {$X$};

		\node at (0.5, -0.5) {$A$};
		\node at (1.5, -0.5) {$B$};
		\node at (2.5, -0.5) {$C$};

		\node at (-0.5, 0.5) {$D$};
		\node at (-0.5, 1.5) {$C$};
		\node at (-0.5, 2.5) {$B$};
	\end{tikzpicture}
	\caption*{\footnotesize{Tabella degli stati distinguibili: in rosso il passo base e in blu il primo passo induttivo}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c | c}
		\begin{tikzpicture}
			\node (p) {$B \xrightarrow{\makebox[0.5cm]{\textit{a}}} A$};
			\node at (0, -0.5) {$D \xrightarrow{\makebox[0.5cm]{\textit{a}}} C$};
			
			\node at(3, -0.3) {$\Rightarrow (B, D)$ distinguibili};
		\end{tikzpicture}&
		\begin{tikzpicture}
			\node {$A \xrightarrow{\makebox[0.5cm]{\textit{b}}} A$};
			\node at (0, -0.5) {$B \xrightarrow{\makebox[0.5cm]{\textit{b}}} C$};
			
			\node at(3, -0.3) {$\Rightarrow (A, B)$ distinguibili};
		\end{tikzpicture}\\
	\end{tabular}
	\caption*{\footnotesize{Esempio di passo induttivo per la coppia di stati $(A, C)$}}
\end{figure}

Posso quindi unire gli stati $C$ e $D$ in quanto indistinguibili:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial] (A) {$A$};
		\node[state] (B) [right=of A] {$B$};
		\node[state, accepting] (CD) [right=of B] {$CD$};
		\path[->]
			(A)  edge [loop above] node {$b$} (A)
			     edge [bend left=20] node {$a$} (B)
			(B)  edge [bend left=20] node {$a$} (A)
			     edge node {$b$} (CD)
			(CD) edge [loop right] node {$a, b$} (CD);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico minimale}}
\end{figure}

\newpage
\subsection{Algoritmo di Brzozowski}
\subsubsection{Minimalità del reversal}
\textsf{Lemma}:\\
sia $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ un automa deterministico in cui tutti gli stati sono raggiungibili e $L$ il linguaggio accettato da $A$.\\
Consideriamo ora $N^{R} = \langle Q, \Sigma, \delta^{R}, F, q_{0} \rangle$ automa non deterministico per $L^{R}$ ottenuto con la costruzione vista nella sezione \ref{sect:lez7-rev} e $A^{R}$ il suo corrispettivo deterministico ottenuto con la costruzione per sottoinsiemi limitato agli stati raggiungibili.\\
Allora $A^{R}$ è l'automa deterministico minimo che accetta $L^{R}$.

\bigskip
\textsf{Dimostrazione}:\\
consideriamo l'automa $A^{R} = \langle Q^{R}, \Sigma, \delta^{R}, q_{0}^{R}, F^{R} \rangle$ dove:
\begin{itemize}
	\item $Q^{R} = 2^{Q}$
	\item $q_0{R} = F$
	\item $F^{R} = \{ q \in q^{R} \mid q_{0} \in q\}$
\end{itemize}
Voglio dimostrare che tutti gli stati sono distinguibili fra di loro: supponiamo per assurdo che ci siano due stati $q_{1}, q_{2} \in Q^{R} \mbox{ con } q_{1} \neq q_{2}$ indistinguibili.\\
Sia $p_{1} \in q_{1}$ uno stato dell'automa di partenza: $\exists w \in \Sigma^{*} \ \ \delta(q_{0}, w) = p$:\\
in $A^{R}$ ho che $\delta^{R}(q_{1}, w^{R}) = \alpha \in F^{R}$ per le costruzioni applicate.\\
Poiché $q_{2}$ è indistinguibile da $q_{1}$ devo avere che $\delta^{R}(q_{2}, w^{R}) = \beta \in F^{R}$: deve pertanto essere vero che $\exists p_{2} \in q_{2} \ \ \delta(q_{0}, w) = p_{1}$.\\
Dato che l'automa di partenza era deterministico deve essere vero che $p = p_{1}$.\\
Ho quindi dimostrato che $q_{2} \subseteq q_{1}$: in maniera simmetrica dimostro $q_{1} \subseteq q_{2}$ e quindi $q_{1} = q_{2} \Rightarrow$ assurdo.

\subsubsection{Algoritmo}
\textsf{Input}: automa deterministico $A$ per $L$\\
\textsf{Output}: automa deterministico minimo $M$ per $L$

\textsf{Passi}:
\begin{enumerate}[label=\protect\circled{\small{\arabic*}}]
	\item Costruisco $N^{R}$ non deterministico per $L^{R}$
	\item Costruisco $A^{R}$ deterministico per $L^{R}$
	\item Costruisco $(N^{R})^{R}$ non deterministico per $(L^{R})^{R} = L$
	\item Costruisco $M	$ deterministico per $(L^{R})^{R} = L$
\end{enumerate}

\newpage
\section{Automi bidirezionali}
Un automa bidirezionale è in grado di muoversi in entrambe le direzioni sul nastro.
Si può inoltre pensare una limitazione a questo modello di calcolo in cui la testina non può cambiare direzione in mezzo alla stringa, ma può procedere solo dall'inizio alla fine e viceversa: questi automi sono detti automi sweeping bidirezionali.

\begin{figure}[ht!]
	\centering
	\begin{tikzpicture}
		% override global `shorten >=` value to remove tiny gap in the top-right corner
		\draw[shorten >=0pt, step=0.4cm,gray,very thin] (0,0) grid (6,0.4);
		\draw[-{>[scale=2]}] (2.2,-1) .. controls (2.2, 0.5) and (1.8, -1.5) .. (1.8,0);
		\draw[<->, line width = 1pt] (3, -0.2) -- (3.6, -0.2);
		\draw (2, -1) rectangle (2.4, -1.4);
	\end{tikzpicture}
\end{figure}

Alla stringa di input vengono inoltre aggiunti due simboli $\triangleright$ e $\triangleleft$ che simboleggiano, rispettivamente, l'inizio e la fine della stringa di input.

\bigskip
\textsf{Esempio}:\\
Consideriamo nuovamente la seguente famiglia di linguaggi:
\[L_{n} = \{ x \in \{a, b\}^{*} \mid \mbox{l'$n$-esimo simbolo da destra di $x$ è una $a$}\}\]
Come abbiamo visto nella sezione \ref{sect:lez4-diff} $SC(L_{n}) \geq 2^{n}$.

Utilizzando un automa bidirezionale sono invece sufficienti circa $n$ stati. Basta infatti utilizzare il seguente algoritmo:
\begin{enumerate}[label=\protect\circled{\small{\arabic*}}]
	\item Arrivo in fondo alla stringa ignorando tutto (uno stato)
	\item Torno indietro di n-1 simboli ($n$ stati)
	\item Controllo che il simbolo subito precedente sia una $a$ (uno stato)
\end{enumerate}
%TODO: esempi/disegnini

\bigskip
\textsf{Esempio}:\\
Consideriamo la seguente famiglia di linguaggi, di cui abbiamo visto un esempio simile nella sezione \ref{sect:lez5-compl}:
\[K_{n} = (a+b)^{*}a(a+b)^{n-1}a(a+b)^{*}\]
%TODO: esempi/disegnini

$SC(K_{n}) \geq 2^{n}$

\newpage
\section{Esercizi a casa}
Scrivere un automa sweeping bidirezionale per i seguenti linguaggi:
\[K_{n} = (a+b)^{*}a(a+b)^{n-1}a(a+b)^{*}\]
