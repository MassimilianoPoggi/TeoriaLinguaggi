\chapter{Lezione 7}
\section{Operazioni sui linguaggi}
\subsection{Prodotto}
\label{sect:lez7-prod}
\subsubsection{Costruzione non deterministica}
\[L_{1} \cdot L_{2} = \{xy \mid x \in L_{1} \wedge y \in L_{2} \}\]

Notiamo che il linguaggio vuoto e il linguaggio costituito solo da $\epsilon$ sono elementi neutri rispetto a quest'operazione.

Dati due automi $A_{1} = \langle Q_{1}, \Sigma, \delta_{1}, q_{0_{1}}, F_{1} \rangle$ e $A_{2} = \langle Q_{2}, \Sigma, \delta_{2}, q_{0_{2}}, F_{2} \rangle$
definisco un nuovo automa $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ collegando gli stati finali del primo con gli stati iniziali del secondo con delle $\epsilon$-mosse:
\begin{itemize}
	\item $Q = Q_{1} \cup Q_{2}$
	\item $\delta(q, a) =
		\begin{cases}
			\delta_{2}(q, a), & \mbox{se } q \in Q_{2}\\
			\delta_{1}(q, a), & \mbox{se } \delta(q, a) \in Q_{1} \setminus F_{1}\\
			\delta_{1}(q, a) \cup q_{0_{2}}, & \mbox{se } \delta(q, a) \in F_{1}
		\end{cases}$
	\item $q_{0} =
		\begin{cases}
			q_{0_{1}}, & \mbox{se } q_{0_{1}} \notin F_{1}\\
			q_{0_{1}} \cup q_{0_{2}}, & \mbox{se } q_{0_{1}} \in F_{1}
		\end{cases}$
	\item $F = F_{2}$
\end{itemize}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=3.5cm]
		\node (start) {start};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=-1cm] (A_1) [right=of start] {$A_{1}$};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt] (A_2) [right=of A_1] {$A_{2}$};

		\path[->]
			(start) edge node {} (A_1)
			(A_1) edge node {$\epsilon$} (A_2);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per il prodotto di due linguaggi}}
\end{figure}
L'automa ottenuto è non deterministico, pertanto:\\
$SC(L_{1} \cdot L_{2}) \leq 2^{SC(L_{1}) + SC(L_{2})}$\\
$NSC(L_{1} \cdot L_{2}) \leq NSC(L_{1}) + NSC(L_{2})$

\bigskip
\textsf{Esempio}:\\
$L_{1} = abaa^{*}, L_{2} = aab^{*} \Rightarrow abaaaaaaab \in L_{1} \cdot L_{2}$

\newpage
\subsubsection{Costruzione deterministica}
Costruzione alternativa per il caso deterministico per $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ (si simulano in parallelo tutte le esecuzioni del secondo automa):
\begin{itemize}
	\item $Q = Q_{1} \times 2^{Q_{2}}$
	\item $q_{0} = 
		\begin{cases}
			(q_{0_{1}}, \emptyset), & \mbox{se } q_{0_{1}} \notin F_{1}\\
			(q_{0_{1}}, \{q_{0_{2}}\}), & \mbox{se } q_{0_{2}} \in F_{1}
		\end{cases}$
	\item $\delta((q, \alpha), a) =
		\begin{cases}
			(\delta_{1}(q, a), \bigcup\limits_{p \in \alpha}\delta_{2}(p, a)), & \mbox{se } \delta_{1}(q, a) \notin F_{1}\\
			(\delta_{1}(q, a), \bigcup\limits_{p \in \alpha}\delta_{2}(p, a) \cup \{q_{0_{2}}\}), & \mbox{se } \delta_{1}(q, a) \in F_{1}\\
		\end{cases}$
	\item $F = \{(q, \alpha) \mid \alpha \cap F_{2} \neq \emptyset\}$
\end{itemize}

\bigskip
$SC(L_{1} \cdot L_{2}) \leq SC(L_{1}) \cdot 2^{SC(L_{2})}$

\bigskip
\textsf{Esempio}:\\
$L_{1} = (a+b)^{*}$\\
$L_{2} = a(a+b)^{n}$

Si osservi che $SC(L_{1}) = 1$ e $SC(L_{2}) = n$, ma $L_{1} \cdot L_{2}$ è il linguaggio visto nella sezione \ref{sect:lez4-diff}, che abbiamo dimostrato aver bisogno di un numero esponenziale di stati.

\newpage
\subsection{Chiusura di Kleene}
\[L^{*} = \bigcup_{k \geq 0} L^{k} \mbox{ dove } L^{k} =
	\begin{cases}
		\{\epsilon\}, & \mbox{se } k = 0\\
		L^{k-1} \cdot L, & \mbox{se } k \geq 1
	\end{cases}\]

Per costruire l'automa collego tutti gli stati finali con lo stato iniziale con una $\epsilon$-mossa. Aggiungo inoltre un nuovo stato iniziale che con una $\epsilon$-mossa va allo stato iniziale per accettare sempre anche la stringa $\epsilon$.

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial, accepting] (q_0) {$q_{0}$};
		\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=0.5cm] (A) [right=of q_0] {$A$};

		\path[->]
			(q_0) edge node {$\epsilon$} (A);

		\draw[->] (A.east) .. controls (5, -1.7) and (0, -1.7) .. (A.west);
		\node at (2.5, -1.5) (epsilon) {$\epsilon$};
	\end{tikzpicture}
	\caption*{\footnotesize{Costruzione dell'automa per la chiusura di Kleene}}2
\end{figure}

$NSC(L^{*}) \leq 1 + NSC(L)$\\
$SC(L^{*}) \leq 2^{1 + SC(L)}$

\bigskip
Anche in questo caso si può fare una costruzione deterministica analoga a quella precedente: il risultato resta però esponenziale come con questa costruzione.

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge [bend left=20] node {$a$} (q_1)
			(q_1) edge [bend left=20] node {$a$} (q_0)
			      edge node {$b$} (q_2);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per il linguaggio $L = (aa)^{*}ab$}}
\end{figure}
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\node[state, initial, accepting] (q_star) [left=of q_0] {$q_{*}$};
		\path[->]
			(q_0)    edge [bend left=20] node {$a$} (q_1)
			(q_1)    edge [bend left=20] node {$a$} (q_0)
			         edge node {$b$} (q_2)
			(q_star) edge node {$\epsilon$} (q_0)
			(q_2)    edge [bend left=50] node {$\epsilon$} (q_0);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per $L^{*}$}}
\end{figure}

\newpage
\subsection{Reversal}
\label{sect:lez7-rev}
Data una stringa $x \in \Sigma^{*}$ definiamo l'operazione di reverse:
\[ (x_{0}x_{1}\dots x_{n})^{R} = x_{n}\dots x_{1}x_{0}\]

A partire da questa operazione definiamo quindi l'operazione di reverse su un linguaggio:
\[L^{R} = \{x^{R} \mid x \in L\}\]

Dato $A_{1} = \langle Q_{1}, \Sigma, \delta_{1}, q_{0_{1}}, F_{1} \rangle$ costruisco un automa
$A = \langle Q, \Sigma, \delta, Q_{0}, F \rangle$ in cui inverto la funzione di transizione e stati iniziali e finali:
\begin{itemize}
	\item $Q = Q_{1}$
	\item $Q_{0} = F_{1}$: ho più stati iniziali
	\item $\delta(q, a) = \{ p \mid \delta_{1}(p, a) = q \}$ 
	\item $F = \{ q_{0_{1}} \}$ 
\end{itemize}

\bigskip
$NSC(L^{R}) = NSC(L)$\\
$SC(L^{R}) \leq 2^{SC(L)}$

\bigskip
Anche qui l'esponenziale è inevitabile:\\
$L = \{ x \in \{a, b\}^{*} \mid \mbox{l'\textit{n}-esimo simbolo di x è una } a\}$\\
$L^{R} = \{ x \in \{a, b\}^{*} \mid \mbox{l'\textit{n}-esimo simbolo da destra di x è una } a\}$\\
$SC(L) = n+1$, ma $SC(L^{R}) = 2^n$

\newpage
\subsection{Shuffle}
Date due stringhe $x, y \in \Sigma^{*}$ costruisco un insieme che rappresenta tutti i possibili modi di mischiare le due stringhe:
\[sh(x, y) = \{ x_{0}b_{1}x_{1}b_{2}\dots b_{k}x_{k} \mid x_{i} \in \Sigma_{1}^{*} : x_{0}x_{1}\dots x_{k} = x\ \wedge\]\[b_{j} \in \Sigma_{2} : b_{1}\dots b_{k} = y\}\]

\textsf{Esempio}:\\
$sh(ab, cd) = \{ abcd, acbd, acdb, cabd, cadb, cdab \}$

\bigskip
Definisco ora la seguente operazione:
\[ sh(L_{1}, L_{2}) = \bigcup_{x \in L_{1}} \bigcup_{y \in L_{2}} sh(x, y)\]

Per costruire un automa tengo le due computazioni in parallelo e per ogni simbolo decido a quale dei due automi originari attribuirlo:
\begin{itemize}
	\item $Q = Q_{1} \times Q_{2}$
	\item $(q_{0_{1}}, q_{0_{2}})$
	\item $\delta((q_{1}, q_{2}), a) = \{ (\delta_{1}(q_{1}, a), q_{2}), (q_{1}, \delta_{2}(q_{2}, a))\}$
	\item $F = F_{1} \times F_{2}$
\end{itemize}

\bigskip
$NSC(sh(L_{1}, L_{2})) \leq NSC(L_{1}) \cdot NSC(L_{2})$

\bigskip
La complessità nel caso deterministico è più complessa: se i due alfabeti sono disgiunti so sempre quale delle due funzioni di transizione utilizzare, pertanto elimino il non determinismo:\\
$SC(sh(L_{1}, L_{2})) \leq 
	\begin{cases}
		SC(L_{1}) \cdot SC(L_{2}), & \mbox{se } \Sigma_{1} \cap \Sigma_{2} = \emptyset\\
		2^{SC(L_{1}) \cdot SC(L_{2})}, & \mbox{se } \Sigma_{1} \cap \Sigma_{2} \neq \emptyset
	\end{cases}$

\newpage
\subsection{Quoziente}
\[L_{1} / L_{2} = \{ x \in \Sigma^{*} \mid \exists y \in L_{2} \ xy \in L_{1} \}\]

\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{l l}
			$L_{1} = a^{+}bc^{+}$&
			$L_{1} / L_{2} = a^{+}$\\

			$L_{2} = bc^{+}$&
			$L_{1} / L_{3} = a^{+}bc^{*}$\\

			$L_{3} = c^{+}$
	\end{tabular}
\end{figure}

Cancellare tutti gli zeri finali dalle stringhe di un linguaggio $L \subseteq \{0, 1\}^{*}$:
\[ L / 0^{*} \cap (\{\epsilon\} \cup (0+1)^{*}1)\]

\bigskip
Dati i linguaggi $L, R \subseteq \Sigma^{*}$, dove $L$ è un qualsiasi linguaggio e $R$ è regolare, se calcoliamo $R / L$ otteniamo un linguaggio regolare.

\bigskip
In particolare se $L \subseteq R$, dato $A_{R} = \langle Q_{R}, \Sigma, \delta_{R}, q_{0_{R}}, F_{R} \rangle$ automa deterministico per $R$ costruisco
$A = \langle Q_{R}, \Sigma, \delta_{R}, q_{0_{R}}, F \rangle$ dove definisco\\
$F = \{ q \in Q \mid \exists y \in L \ \delta_{R}(q, y) \in F_{R}\}$\\
Si noti che nel caso generale per $L \not\subseteq R$ non posso fornire un processo costruttivo per gli stati finali senza conoscere il linguaggio $L$.

\bigskip
\textsf{Esempio} (assumendo $y \in R$):
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge node {$x$} (q_1)
			(q_1) edge node {$y$} (q_2);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa originario}}
\end{figure}
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial] (q_0) {$q_{0}$};
		\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
		\node[state] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge node {$x$} (q_1)
			(q_1) edge node {$y$} (q_2);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa del quoziente}}
\end{figure}

\newpage
\subsection{Morfismo}
Dati due alfabeti $\Sigma, \Delta$ e una funzione $h: \Sigma \to \Delta^{*}$ e una stringa $x \in \Sigma^{*}$ definiamo la seguente operazione:
\[h(x) = 
	\begin{cases}
		\epsilon, & \mbox{se } x = \epsilon\\
		h(y)h(\sigma), & \mbox{se } x = y\sigma, y \in \Sigma^{*}, \sigma \in \Sigma
	\end{cases}\]

Posso quindi estendere l'operazione di morfismo ad un linguaggio:
\[h(L) = \{ h(x) \mid x \in L \}\]

Per costruire l'automa sostituisco le transizioni dell'automa originario con dei cammini per simulare la trasformazione $h(\sigma)$.

\bigskip
\textsf{Esempio}:\\
$\Sigma = \{a, b\}, \Delta = \{0, 1\}, h(a) = 01, h(b) = 1$

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial, accepting] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\path[->]
			(q_0) edge [bend left=20] node {$a$} (q_1)
			(q_1) edge [bend left=20] node {$b$} (q_0)
			      edge [loop right] node {$a$} (q_1);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per $L = ab^{*}$}}
\end{figure}
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state, initial, accepting] (q_0) {$q_{0}$};
		\node[state] (q_a1) [above right=of q_0] {$q_{a1}$};
		\node[state] (q_1) [below right=of q_a1] {$q_{1}$};
		\node[state] (q_a2) [right=of q_1] {$q_{a2}$};
		\path[->]
			(q_0)       edge [bend left=30] node {$0$} (q_a1.west)
			(q_a1.east) edge [bend left=30] node {$1$} (q_1)
			(q_1)       edge node {$1$} (q_0)
			            edge [bend left=20] node {$0$} (q_a2)
			(q_a2)      edge [bend left=20] node {$1$} (q_1);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per $h(L)$}}
\end{figure}

Si noti che l'automa ottenuto è ancora deterministico: questo è perché non ci sono trasformazioni che iniziano con gli stessi simboli e nessun simbolo viene cancellato dalla funzione $h$. Formalmente:
\[\forall x, y \in L \ \ Prefissi(h(x)) \cap Prefissi(h(y)) = \{\epsilon\} \wedge \epsilon \notin Im(h)\]

\newpage
\subsection{Sostituzione}
Dati due alfabeti $\Sigma, \Delta$ e una funzione $s: \Sigma \to 2^{\Delta^{*}}$ definisco operazioni analoghe alle precedenti: invece che sostituire un simbolo con una stringa sto sostituendo un simbolo con un linguaggio.

In questo caso ottengo un linguaggio regolare se $L$ è regolare e se tutti i linguaggi $L_{\sigma} \in Im(s)$ sono regolari.

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.3\textwidth}
			\begin{tikzpicture}
				\node[state] (q) {$q$};
				\node[state] (p) [right=of q] {$p$};
				\path[->] (q) edge node {$a$} (p);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.6\textwidth}
			\begin{tikzpicture}
				\node[state] (q) {$q$};
				\node[rectangle, draw, minimum width=60pt, minimum height=35pt, xshift=0.5cm] (sa) [right=of q] {$s(a)$};
				\node[state] (p) [right=of sa, xshift=0.5cm] {$p$};
				\path[->] 
					(q)  edge node {$\epsilon$} (sa)
					(sa) edge node {$\epsilon$} (p);
			\end{tikzpicture}
		\end{subfigure}
	\end{tabular}
	\caption*{\footnotesize{Trasformazione di una transizione}}
\end{figure}

\textsf{Esempio}:\\
$\Sigma = \{a, b\}, \Delta = \{0, 1\}, s(a) = (01+0)^{*}, s(b) = 1$\\
$L = (ab)^{*}(a+b) \Rightarrow s(L) = ((01+0)^{*}1)^{*}((01+0)^{*}+1)$

\bigskip
Dato il linguaggio $L \subseteq \{a, b\}^{*}$ voglio aggiungere un numero arbitrario di $c$ in qualsiasi posizione:\\
$s(a) = c^{*}ac^{*}, s(b) = c^{*}bc^{*}$

$L_{c} = 
	\begin{cases}
		s(L), & \mbox{se } \epsilon \notin L\\
		s(L) \cup c^{*}, & \mbox{se } \epsilon \in L
	\end{cases}$

\bigskip
Potevo ottenere lo stesso risultato con lo shuffle: $L_{c} = sh(L, c^{*})$
