\chapter{Lezione 8}
\section{Operazioni sui linguaggi}
\subsection{Proiezione}
Dati due alfabeti $\Sigma$ e $\Sigma_{1} \subseteq \Sigma$ definiamo l'operazione di proiezione su una stringa:
\[\pi_{\Sigma_{1}}(x) =
	\begin{cases}
		\epsilon, & \mbox{se } x = \epsilon\\
		y\sigma, & \mbox{se } x=y\sigma, \sigma \in \Sigma_{1}, y \in \Sigma^{*}\\
		y, & \mbox{se } x=y\sigma, \sigma \in (\Sigma \setminus \Sigma_{1}), y \in \Sigma^{*}
	\end{cases}\]

A partire da questa possiamo definire un'operazione analoga sui linguaggi:
\[\pi_{\Sigma'}(L) = \{ \pi_{\Sigma'}(x) \mid x \in L\}\]

\textsf{Esempio}:\\
$\Sigma = \{a, b, c\}$, $\Sigma' = \{a, b\}$\\
$\pi_{\Sigma'}(abccabc) = abab$

\bigskip
Trasformo tutte le transizioni sull'automa con simboli di $\Sigma \setminus \Sigma'$ in $\epsilon$-mosse.

$NSC(\pi_{\Sigma'}(L)) = NSC(L)$
$SC(\pi_{\Sigma'}(L)) = 2^{SC(L)}$

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state,initial] (q_0) {$q_{0}$};
		\node[state] (q_c) [right=of q_0]{$q_c$};
		\node[state] (q_1) [right=of q_c] {$q_{1}$};
		\node (q_i) [right=of q_1] {\dots};
		\node[state] (q_n-1) [right=of q_i] {$q_{n-1}$};
		\node[state, accepting] (q_n) [right=of q_n-1] {$q_{n}$};
		\path[->]
			(q_0)   edge [loop above] node {$a, b$} (q_0)
			        edge node {$c$} (q_c)
			(q_c)   edge node {$a$} (q_1)
			(q_1)   edge node {$a, b$} (q_i)
			(q_i)   edge node {$a, b$} (q_n-1)
			(q_n-1) edge node {$a, b$} (q_n);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa deterministico per un linguaggio $L_{n}$}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state,initial] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node (q_i) [right=of q_1] {\dots};
		\node[state] (q_n-1) [right=of q_i] {$q_{n-1}$};
		\node[state, accepting] (q_n) [right=of q_n-1] {$q_{n}$};
		\path[->]
			(q_0)   edge [loop above] node {$a, b$} (q_0)
			        edge node {$a$} (q_1)
			(q_1)   edge node {$a, b$} (q_i)
			(q_i)   edge node {$a, b$} (q_n-1)
			(q_n-1) edge node {$a, b$} (q_n);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa non deterministico per $\pi_{\{a, b\}}(L_{n})$}}
\end{figure}

Osserviamo che $SC(\pi_{\{a, b\}}(L_{n})) = 2^{n}$ per quanto osservato nella sezione \ref{sect:lez4-diff}: non è quindi possibile fare meglio di un limite superiore esponenziale.

\section{Relazioni di equivalenza}
\subsection{Definizioni basilari}
Dato un insieme $S$, si definisce $R \subseteq S \times S$ relazione su $S$.
La relazione $R$ si dice di equivalenza se soddisfa tre proprietà:
\begin{itemize}
	\item \textit{riflessiva}: $\forall x \in S \ \ \ R(x, x)$ 
	\item \textit{simmetrica}: $\forall x, y \in S \ \ \ R(x, y) \Rightarrow R(y, x)$
	\item \textit{transitiva}: $\forall x, y, z \in S \ \ \ R(x, y) \wedge R(y, z) \Rightarrow R(x, z)$
\end{itemize}

Una relazione di equivalenza $R$ induce una partizione su $S$: divide l'insieme in sottoinsiemi di elementi equivalenti fra loro.\\
Ciascun sottoinsieme è chiamato \textit{classe di equivalenza}, ed è indicato con $[x]$ dove $x$ è un elemento appartenente alla classe di equivalenza.\\
Il numero di classi indotte sull'insieme è detto indice della relazione di equivalenza.

\bigskip
\textsf{Esempio}:\\
$R = \{(x, y) \mid x \mbox{ congruo a } y \mbox{ modulo } 3 \}, S / R = \{ [0], [1], [2] \}, Indice(R) = 3$

\bigskip
La relazione $R$ si dice \textit{invariante a destra} se (data un'operazione di prodotto sull'insieme):
\[ R(x, y) \Rightarrow \forall z \in S \ \ \ R(xz, yz) \]

Date due relazioni di equivalenza $R_{1}, R_{2}$, la relazione $R_{1}$ si dice \textit{raffinamento} di $R_{2}$ se e solo se ogni classe di equivalenza di $R_{1}$ è contenuta in una classe di equivalenza di $R_{2}$.\\
Definizione alternativa: ogni classe di equivalenza di $R_{2}$ è un'unione di classi di equivalenza di $R_{1}$.
Si osservi che $R_{1}(x, y) \Rightarrow R_{2}(x, y)$.

\bigskip
\textsf{Esempio}:\\
$R = \{(x, y) \mid x \mbox{ congruo a } y \mbox{ modulo 3}\}\\
R_{1} = \{(x, y) \mid x \mbox{ congruo a } y \mbox{ modulo 6}\}\\
R_{2} = \{(x, y) \mid x \mbox{ congruo a } y \mbox{ modulo 2}\}$

\bigskip
$R_{1}$ è un raffinamento sia di $R$ che di $R_{2}$

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.4\textwidth}
		\centering
		\begin{tikzpicture}[line width=1pt, shorten >= 0pt]
			\draw (0,0) circle [radius=1.3];
			\draw [color=blue] (0,0) -- (60:1.3);
			\draw [color=blue] (0,0) -- (180:1.3);
			\draw [color=blue] (0,0) -- (300:1.3);
			\draw [color=red] (0:1.3) -- (0,0) -- (120:1.3);
			\draw [color=red] (120:1.3) -- (0,0) -- (240:1.3);

			\node at (30:0.8) {3};
			\node at (90:0.8) {0};
			\node at (150:0.8) {1};
			\node at (210:0.8) {4};
			\node at (270:0.8) {2};
			\node at (330:0.8) {5};
		\end{tikzpicture}
		\caption*{\footnotesize{Relazioni $R$ (in rosso) e $R_{1}$ (in blu)}}
	\end{subfigure}
	\hspace*{1cm}
	\begin{subfigure}{0.4\textwidth}
		\centering
		\begin{tikzpicture}[line width=1pt, shorten >= 0pt]
			\draw (0,0) circle [radius=1.3];
			\draw [color=blue] (60:1.3) -- (0,0) -- (120:1.3);
			\draw [color=blue] (240:1.3) -- (0,0) -- (300:1.3);
			\draw [color=red] (0:1.3) -- (0,0) -- (180:1.3);

			\node at (30:0.8) {0};
			\node at (90:0.8) {2};
			\node at (150:0.8) {4};
			\node at (210:0.8) {1};
			\node at (270:0.8) {3};
			\node at (330:0.8) {5};
		\end{tikzpicture}
		\caption*{\footnotesize{Relazioni $R_{2}$ (in rosso) e $R_{1}$ (in blu)}}
	\end{subfigure}
\end{figure}

\newpage
\subsection{Relazioni su automi}
Dato un automa $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ definisco la seguente relazione di equivalenza sull'automa:
\[R_{A} \subseteq \Sigma^{*} \times \Sigma^{*} = \{ (x, y) \mid \delta(q_{0}, x) = \delta(q_{0}, y) \}\]

L'indice della relazione è uguale al numero di stati dell'automa: $R_{A} = \#Q$.\\
$R_{A}$ è inoltre invariante a destra: $\delta(q_{0}, x) = \delta(q_{0}, y) \Rightarrow \delta(q_{0}, xz) = \delta(q_{0}, yz)$\\
Il linguaggio $L_{A}$ accettato dall'automa $A$ è l'unione delle classi di equivalenza di $R_{A}$ che corrispondono agli stati finali $F$.

\bigskip
\textsf{Esempio}:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[node distance=3cm]
		\node[state, initial, accepting] (q_0) {$q_{0}$};
		\node[state] (q_1) [right=of q_0] {$q_{1}$};
		\node[state, accepting] (q_2) [below=of q_0] {$q_{2}$};
		\node[state, accepting] (q_3) [right=of q_2] {$q_{3}$};
		\path[->]
			(q_0) edge [bend left=20] node {$a$} (q_1)
			      edge [bend left=20] node {$b$} (q_2)
			(q_1) edge [bend left=20] node {$a$} (q_0)
			      edge [bend left=20] node {$b$} (q_3)
			(q_2) edge [bend left=20] node {$a$} (q_3)
			      edge [bend left=20] node {$b$} (q_0)
			(q_3) edge [bend left=20] node {$a$} (q_2)
			      edge [bend left=20] node {$b$} (q_1);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per stringhe con un numero di $a$ pari o di $b$ dispari}}
\end{figure}

Relazione corrispondente all'automa $A$:
\[R_{A} = \{ (x, y) \mid \charcount{a}{x} \% 2 = \charcount{a}{y} \% 2 \ \wedge \ \charcount{b}{x} \% 2 = \charcount{b}{y} \% 2\}\]

\subsection{Relazioni su linguaggi}
Dato un linguaggio $L$ definisco la seguente relazione di equivalenza sull'alfabeto del linguaggio:
\[R_{L} = \{ (x, y) \mid \forall z \in \Sigma^{*} \ \ \ xz \in L \iff yz \in L \}\]
Si osservi che questa relazione è l'esatto contrario della distinguibilità fra stringhe sul linguaggio $L$: due stringhe sono in relazione se e solo se sono indistinguibili.
Come per la relazione della sezione precedente, anche questa è invariante a destra: basta concatenare i due suffissi.
\[R_{L}(x, y) \Rightarrow R_{L}(xz, yz) \Rightarrow (x(zw) \in L \iff y(zw) \in L)\]

Notiamo inoltre che $R_{L}(x, y) \Rightarrow x, y \in L \lor x, y \in L^{C}$: $L$ è l'unione di alcune classi di equivalenza di $R_{L}$.

\section{Minimalità di automi}
\subsection{Teorema di Myhill-Nerode}
Dato un linguaggio $L \subseteq \Sigma^{*}$ le seguenti affermazioni sono equivalenti:
\begin{enumerate}[label=(\alph*)]
	\item $L$ è regolare
	\item $L$ è l'unione di alcune classi di equivalenza di una relazione di equivalenza invariante a destra e di indice finito
	\item la relazione $R_{L}$ vista precedentemente ha indice finito
\end{enumerate}

\textsf{Dimostrazione}:
\begin{itemize}
	\item $(a) \Rightarrow (b)$: è sufficiente considerare la relazione sull'automa del linguaggio vista nella sezione precedente.
	\item $(b) \Rightarrow (c)$: sia $E \subseteq \Sigma^{*} \times \Sigma^{*}$ una relazione di equivalenza invariante a destra di indice finito tale che $L$ è l'unione di classi di equivalenza di $E$.\\
		$(\forall z \in \Sigma^{*} \ \ \ E(xz, yz)) \Rightarrow (\forall z \in \Sigma^{*} \ \ \ xz \in L \iff yz \in L) \Rightarrow R_{L}(x, y)$.\\
		$E$ è un raffinamento di $R_{L}$, quindi l'indice di $E$ è maggiore o uguale dell'indice di $R_{L}$: essendo l'indice di $E$ finito anche l'indice di $R_{L}$ è finito.
	\item $(b) \Rightarrow (a)$: costruisco un automa $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ dove:
		\begin{itemize}
			\item $Q = \{ [x] \mid x \in \Sigma^{*} \}$
			\item $q_{0} = [\epsilon]$
			\item $F = \{ [x] \mid x \in L \} $
			\item $\delta([x], a) = [xa]$
		\end{itemize}
		Per induzione si dimostra che $\forall x \in \Sigma^{*} \ \ \ \delta(q_{0}, x) = \delta([\epsilon], y) = [y]$,\\
		e che quindi $[y] \in F \iff y \in L$.
\end{itemize}

\subsection{Teorema dell'automa minimo}
\label{sect:lez8-minim}
L'automa deterministico minimo per un linguaggio $L$ è unico a meno di isomorfismo (cambiare il nome degli stati) ed è l'automa $A$ ottenuto nella dimostrazione del teorema di Myhill-Nerode.

Due stati $p, q \in Q$ sono indistinguibili (o equivalenti) se e solo se\\
$\forall z \in \Sigma^{*} \ \ \ \delta(q, z) \in F \iff \delta(p, z) \in F$  

Algoritmo per la minimizzazione: individuo tutti gli stati distinguibili per induzione.
\begin{itemize}
	\item $q \in F \wedge q \notin F \Rightarrow q, p$ sono stati distinguibili
	\item dati $q, p$ stati distinguibili: $\forall a \in \Sigma, q_{1}, p_{1} \in Q \ (\delta(q_{1}, a) = q \wedge \delta(p_{1}, a) = p) \Rightarrow q_{1}, p_{1}$ sono distinguibili 
\end{itemize}

\section{Esercizi a casa}
\subsection{Estrazione dei pari}
\`{E} dato il linguaggio che prende solo i simboli pari di una stringa:
\[P(L) = \{ w \in \{a, b\}^{*} \mid \exists y \in L : \mbox{ i simboli in posizione pari formano w}\}\]

Costruire automa per $P(L)$ a partire da automa per $L$.

\bigskip
\textsf{Esempio}:\\
$L = (ab)^{*}, P(L) = b^{*}$	

\bigskip
\textsf{Soluzione}:\\
A partire da $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ per $L$ definisco un nuovo automa\\
$A_{P} = \langle Q, \Sigma, \delta_{P}, q_{0}, F_{P} \rangle$ modificando funzione di transizione e stati finali:
\begin{itemize}
	\item la nuova funzione di transizione simula il salto della lettura dei simboli dispari. Formalmente:
		\[\delta_{P}(q, a) = \bigcup_{q_{i} \in \{\delta(q, \sigma) \mid \sigma \in \Sigma\}}\delta(q_{i}, a)\]
	\item $F_{P} = F \cup \{q \in Q \mid \exists a \in \Sigma \ \ \delta(q, a) \in F\}$
\end{itemize}

Da un automa deterministico ottengo potenzialmente un automa non deterministico, pertanto:\\
$NSC(P(L)) = NSC(L)$\\
$SC(P(L)) = 2^{SC(L)}$

\newpage
\subsection{Rotazione}
\label{sect:lez8-es2}
\`{E} dato il linguaggio che fa la rotazione di una stringa:
\[Cycle(L) = \{ x_{1}x_{2} \mid x_{1}, x_{2} \in \Sigma^{*} \wedge x_{2}x_{1} \in L \}\]

Costruire automa per $Cycle(L)$ a partire da automa per $L$.

\bigskip
\textsf{Esempio}:\\
$Cycle({abb}) = \{abb, bba, bab\}$

\bigskip
\textsf{Soluzione}:\\
L'idea è di simulare in parallelo tutti i possibili punti in cui posso spaccare una stringa.\\
Dato l'automa $A = \langle Q, \Sigma, \delta, q_{0}, F \rangle$ per $L$ costruisco due automi $A_{q}, B_{q}$ per ogni stato $q \in Q$:
\begin{itemize}
	\item $Q_{B} = Q_{A} = Q$
	\item $q_{0_{A}} = q_{0}$, $q_{0_{B}} = q$
	\item $F_{A} = \{q\}$, $F_{B} = F$
	\item $\delta_{B} = \delta_{A} = \delta$
\end{itemize}

Applico ora l'operazione di prodotto per costruire l'automa $BA_{q}$ e poi l'operazione di unione per unire i $\#Q$ automi del prodotto in un unico automa.

\bigskip
Ripetendo le considerazioni effettuate nelle sezioni \ref{sect:lez6-union} e \ref{sect:lez7-prod} possiamo quindi concludere che $NSC(Cycle(L)) = 2 \cdot NSC^{2}(L)$

\bigskip
Per gli automi deterministici facciamo l'analisi passaggio per passaggio:
\begin{itemize}
	\item ogni automa $BA_{q}$ è il prodotto di due automi di complessità $SC(L)$: la sua complessità è quindi $SC(L) \cdot 2^{SC(L)}$
	\item devo fare l'unione di $SC(L)$ automi di complessità $C$: estendendo il ragionamento visto nella sezione \ref{sect:lez6-union} la complessità risultante è $C^{SC(L)}$
\end{itemize}
$SC(Cycle(L)) = (SC(L) \cdot 2^{SC(L)})^{SC(L)} = SC^{SC(L)}(L) \cdot 2^{SC^{2}(L)}$

\newpage
\`{E} possibile fornire anche una costruzione alternativa per l'automa per\\
$Cycle(L)$ che segue la stessa idea.\\
Si consideri l'automa $C = \langle Q_{C}, \Sigma, \delta_{C}, q_{0_{C}}, F_{C} \rangle$ costruito nel seguente modo:
\begin{itemize}
	\item $Q_{C} = Q \times Q \times \{\mbox{\textit{sì}}, no\}$. La tripla $(p, q, r)$ assume il seguente significato:
		\begin{itemize}
			\item $p$ rappresenta lo stato corrente di esecuzione
			\item $q$ rappresenta lo stato in cui la stringa è stata spezzata
			\item $r$ indica se sono passato dallo stato iniziale
		\end{itemize}
	\item $q_{0_{C}} = \{ (q, q, no) \mid q \in Q \}$
	\item $F_{C} = \{ (q, q, \mbox{\textit{sì}}) \mid q \in Q \}$
	\item La funzione di transizione simula l'automa e si ricorda se è passato dallo stato finale: posso inoltre effettuare una $\epsilon$-mossa da uno stato finale allo stato iniziale. Formalmente:
		\[\delta_{C}((q, p, r), a) =
			\begin{cases}
				(\delta(p, a), q, r) \cup (q_{0}, q, \mbox{\textit{sì}}), & \mbox{se } r = no \ \wedge \ \delta(p, a) \in F\\
				(\delta(p, a), q, r), & \mbox{altrimenti}\\
			\end{cases}\]
\end{itemize}

Anche in questa costruzione $NSC(Cycle(L)) = 2 \cdot NSC^{2}(L)$, ma non è possibile fare un calcolo preciso per $SC(L)$: posso quindi solo concludere $SC(Cycle(L)) = 2^{2 \cdot SC^{2}(L)}$, valore superiore rispetto a quello trovato con la costruzione precedente.

\newpage
\subsection{Reversal}
Dato l'automa $A$:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}[state/.style={circle, draw, minimum size=1.1cm}]
		\node[state, initial] (q_0) {$q_{0}$};
		\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
		\node[state, accepting] (q_2) [right=of q_1] {$q_{2}$};
		\path[->]
			(q_0) edge [loop above] node {$1$} (q_0)
			      edge node {$0$} (q_1)
			(q_1) edge [loop above] node {$0$} (q_1)
			      edge [bend left=20] node {$1$} (q_2)
			(q_2) edge [loop above] node {$0$} (q_2)
			      edge [bend left=20] node {$1$} (q_1);
	\end{tikzpicture}
	\caption*{\footnotesize{Automa per il linguaggio $L_{A}$}}
\end{figure}

Costruire l'automa per il reversal e renderlo deterministico.\\
Costruire dall'automa ottenuto l'automa per il reversal e renderlo deterministico.

\bigskip
\textsf{Soluzione}:
\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, accepting] (q_0) {$q_{0}$};
				\node[state, initial] (q_1) [left=of q_0] {$q_{1}$};
				\node[state, initial] (q_2) [below=of q_1] {$q_{2}$};
				\path[->]
					(q_0) edge [loop above] node {$1$} (q_0)
					(q_1) edge [bend left=20] node {$1$} (q_2)
					      edge node {$0$} (q_0)
					      edge [loop above] node {$0$} (q_1)
					(q_2) edge [loop right] node {$0$} (q_2)
					      edge [bend left=20] node {$1$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial] (q_1_2) {$q_{1,2}$};
				\node[state, accepting] (q_0_1_2) [right=of q_1_2] {$q_{0,1,2}$};
				\path[->]
					(q_1_2)   edge [loop above] node {$1$} (q_1_2)
					          edge node {$0$} (q_0_1_2)
					(q_0_1_2) edge [loop above] node {$0,1$} (q_0_1_2);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa non deterministico per $L^{R}$}&
		\footnotesize{Automa deterministico per $L^{R}$}\\
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c c}
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial] (q_0) {$q_{0}$};
				\node[state, accepting] (q_1) [right=of q_0] {$q_{1}$};
				\path[->]
					(q_0) edge [loop above] node {$0,1$} (q_0)
					      edge node {$0$} (q_1)
					(q_1) edge [loop above] node {$1$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\begin{tikzpicture}
				\node[state, initial] (q_0) {$q_{0}$};
				\node[state, accepting] (q_1) [right=of q_1_2] {$q_{0,1}$};
				\path[->]
					(q_0) edge [loop above] node {$1$} (q_1_2)
					      edge node {$0$} (q_0_1_2)
					(q_1) edge [loop above] node {$0,1$} (q_0_1_2);
			\end{tikzpicture}
		\end{subfigure}\\
		\footnotesize{Automa non deterministico per $(L^{R})^{R}$}&
		\footnotesize{Automa deterministico per $(L^{R})^{R}$}\\
	\end{tabular}
\end{figure}

\textsf{Osservazione}:\\
Abbiamo ottenuto una semplificazione dell'automa di partenza, nonostante le trasformazioni per il reversal possano potenzialmente portare ad un salto esponenziale.
