\chapter{Lezione 3}
\section{Classificazione delle grammatiche}
Le grammatiche possono essere classificate in base alla forma delle produzioni, costituendo tipica gerarchia di Chomsky, composta da 4 tipi:
\begin{itemize}
	\item Tipo 0 (\textit{ricorsivamente numerabili}): nessuna restrizione
	\item Tipo 1 (\textit{context-sensitive}): $\forall \alpha \to \beta \in P \ |\alpha| \leq |\beta|$\\
		Si può usare anche una definizione alternativa, che mostra perché le grammatiche di questo tipo sono chiamate \textit{context-sensitive}:\\
		$\alpha_{1}A\alpha_{2} \to \alpha_{1}\beta\alpha_2{}$ con $ \alpha_{1}, \alpha_{2} \in (V \cup \Sigma)^{*}, A \in V, \beta \in (V \cup \Sigma)^{+}$
	\item Tipo 2 (\textit{context-free}): $A \to \beta$ con $A \in V, \beta \in (V \cup \Sigma)^{+}$\\
		Si noti che queste rappresentano effettivamente una specializzazione delle grammatiche di tipo 1 in cui non c'è più contesto (proprio come suggerisce il nome dato a questa categoria)
	\item Tipo 3 (\textit{regolari}): le produzioni possono assumere solo due forme:
		\begin{itemize}
			\item $A \to aB$ con $A, B \in V, a \in \Sigma$
			\item $A \to a$ con $A \in V, a \in \Sigma$
		\end{itemize}
\end{itemize}
\begin{figure}[!hb]
	\centering
	\begin{tikzpicture}
		\draw (.1,0) circle (.7) node[] {$Tipo\ 3$};
		\draw (.64,0) ellipse (1.48 and 1.05) node[right=.18] {$Tipo\ 2$};
		\draw (1.3,0) ellipse (2.39 and 1.6) node[right=1.05] {$Tipo\ 1$};
		\draw (1.95,0) ellipse (3.295 and 2.15) node[right=1.95] {$Tipo\ 0$};
	\end{tikzpicture}
\end{figure}

\textsf{Osservazioni}:
\begin{itemize}
	\item come appare anche graficamente, ogni tipo è contenuto interamente dal tipo precedente, e per ogni tipo esiste almeno un linguaggio che non appartiene al tipo precedente
	\item proiettando quanto appena visto nei linguaggi, un linguaggio $L \subseteq \Sigma^{*}$ è di tipo \textit{i} $\iff \exists G$ grammatica di tipo \textit{i} che genera $L$
\end{itemize}

\subsection{Problema della parola vuota}
Per come sono stati definiti i tipi fin'ora, solo le grammatiche di tipo 0 possono generare linguaggi contenenti la parola vuota.

Si consideri un linguaggio $L \subseteq \Sigma^{+}$ ed lo si estenda in modo che possa contenere anche la parola vuota, ottenendo $L' = L \cup \{\epsilon\}$.
In questo caso, nella grammatica corrispondente non è sufficiente aggiungere la produzione $S \to \epsilon$, in quanto potrebbero emergere delle problematiche se \textit{S} dovesse apparire in altre produzioni.

\bigbreak
\textsf{Esempio}:\\
$P = \{S \to aSb, S \to c\}$

Il linguaggio genera le stringhe della forma $\{a^{n}cb^{n} \mid n \in \mathbb{N}\}$, ma aggiungere $S \to \epsilon$ aggiunge anche le stringhe non contenenti \textit{c}.

\bigbreak
\textsf{Soluzione}:\\
occorre generare una nuova grammatica, cambiando i nomi delle variabili. Formalmente:
\[G' = \langle V \cup \{ S' \}, \Sigma, P \cup \{ S' \to \epsilon \} \cup \{ S' \to \alpha \mid S \to \alpha \in P \}, S' \rangle \]

\textsf{Osservazioni}:
\begin{itemize}
	\item per comodità, verrà permesso che $A \to \beta$ con $\beta \in (V \cup \Sigma)^{*}$, sapendo che esiste un modo per trasformarle in grammatiche con le caratteristiche descritte sopra
	\item l'$\epsilon$-produzione può essere applicata esclusivamente al simbolo iniziale, in cui si può procedere con la cancellazione
\end{itemize}

\newpage
\section{Linguaggi context-sensitive}
Si vuole dimostrare che i linguaggi context-sensitive sono ricorsivi, ovvero esiste una macchina che, data una stringa $x \in \Sigma^{*}$, risponde nel modo seguente:
\[
	M(x) = 
		\begin{cases}
			\mbox{Sì}, & \mbox{se } x \in L \\
			\mbox{No}, & \mbox{se } x \notin L \\
		\end{cases}
\]

Si prenda in considerazione una stringa $x$ con $|x| = n$ e i seguenti insiemi:
\[T_{m} = \{\gamma \in \bigcup_{i=0}^{n}(V \cup \Sigma)^{i} \mid \exists j \in [0, m] : S \xRightarrow{j} \gamma \} \mbox{ con } m \in \mathbb{N}\]

Si costruiscano gli insiemi per induzione:
\begin{itemize}
	\item $T_{0} = \{ S \}$
	\item $T_{m} = T_{m-1} \cup \{ \gamma \in (V \cup \Sigma)^{*} \mid |\gamma| \leq n \wedge \exists \beta \in T_{m-1} : \beta \Rightarrow \gamma \}$
\end{itemize}

\textsf{Osservazioni}:
\begin{itemize}
	\item dal momento che la lunghezza delle stringhe cresce, $\exists m \in \mathbb{N} : T_{m-1} = T_{m}$
	\item poiché $T_{m}$ contiene tutte le stringhe $y \in L : |y| \leq n$, si può concludere che $x \in L \iff x \in T_{m}$
\end{itemize}

Un discorso analogo può essere fatto per le grammatiche di tipo 0, ma non avendo limitazione sulla lunghezza delle produzioni non è possibile stabilire il passo a cui fermarsi.
\newpage
\section{Linguaggi regolari}
A questo punto, si esplorino alcuni costrutti che hanno la stessa potenza riconoscitiva dei linguaggi regolari.
\subsection{Automi a stati finiti}
Un automa a stati finiti è una macchina che risponde al problema dell'appartenenza di una stringa ad un linguaggio. L'automa possiede un nastro di input, da cui legge la stringa, e un'unità di controllo, che mantiene lo stato corrente e muove la testina sul nastro da sinistra verso destra.
Una volta terminata la stringa risponde se essa appartiene al linguaggio o meno.

\vspace*{-5pt}
\begin{figure}[ht!]
	\centering
	\begin{tikzpicture}
		% override global `shorten >=` value to remove tiny gap in the top-right corner
		\draw[shorten >=0pt, step=0.4cm,gray,very thin] (0,0) grid (6,0.4);
		\draw[-{>[scale=2]}] (2.2,-1) .. controls (2.2, 0.5) and (1.8, -1.5) .. (1.8,0);
		\draw[->, line width = 1pt] (3, -0.2) -- (3.6, -0.2);
		\draw (2, -1) rectangle (2.4, -1.4);
	\end{tikzpicture}
\end{figure}
\vspace*{-5pt}

Un automa $A$ è una quintupla $\langle Q, \Sigma, \delta, q_{0}, F \rangle$ dove:
\begin{itemize}
	\itemsep\itemspacing
	\item $Q$ è l'insieme degli stati
	\item $\Sigma$ è l'alfabeto di input
	\item $\delta: Q \times \Sigma \to Q$ è la funzione di transizione
	\item $q_{0} \in Q$ è lo stato iniziale
	\item $F \subseteq Q$ è l'insieme degli stati di accettazione
\end{itemize}

\textsf{Esempio}:\\
automa che riconosce le parole contenenti un numero pari di $a$
\begin{itemize}
	\itemsep\itemspacing
	\item $Q = \{ q_{0}, q_{1} \}$
	\item $\Sigma = \{ a, b \}$
	\item $\delta$ definita come in tabella
	\item $q_{0} = q_{0}$
	\item $F = \{ q_{0} \}$
\end{itemize}

\begin{figure}[!ht]
	\centering
	\begin{tabular}{c @{\hskip 1cm} c}
		\begin{subfigure}{0.4\textwidth}
			\centering
			\begin{tikzpicture}
				\node[state,initial,accepting] (q_0) {$q_0$};
				\node[state] (q_1) [right=of q_0] {$q_1$}; 
				\path[->]
					(q_0) edge [loop below] node {$b$} (q_0)
					      edge [bend left = 30] node {$a$} (q_1)
					(q_1) edge [bend left = 20] node {$a$} (q_0)
					      edge [loop below] node {$b$} (q_1);
			\end{tikzpicture}
		\end{subfigure}&
		\begin{subfigure}{0.4\textwidth}
			\centering
			\begin{tabular}{l | l l}
				$\delta$ & $a$ & $b$\\ \hline
				$q_{0}$ & $q_{1}$ & $q_{0}$ \\
				$q_{1}$ & $q_{0}$ & $q_{1}$ \\
			\end{tabular}
		\end{subfigure}\\
		\footnotesize{Rappresentazione dell'automa}&
		\footnotesize{Tabella delle transizioni}\\
	\end{tabular}
\end{figure}

\newpage
\textsf{Esempio}:\\
automa che riconosce tutte le stringhe che contengono un numero pari di $a$ fra due $b$ consecutive
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (q_0)	 {$q_0$}; 
		\node[state, accepting] (q_1) [right=of q_0] {$q_1$}; 
		\node[state, accepting] (q_2) [right=of q_1] {$q_2$};
		\node[state, color=red] (q_3) [right=of q_2] {$q_3$};
		\path[->]
			(q_0) edge [loop below] node {$a$} (q_0)
			      edge node {$b$} (q_1)
			(q_1) edge [bend left = 20] node {$a$} (q_2)
			      edge [loop below] node {$b$} (q_1)
			(q_2) edge [bend left = 20] node {$a$} (q_1)
			      edge [color=red] node {$b$} (q_3)
			(q_3) edge [color=red, loop right] node {$a, b$} (q_3);
	\end{tikzpicture}
	\caption*{\footnotesize{La parte in rosso (detta \textit{stato trappola}) viene spesso esclusa per comodità.}}
\end{figure}

\subsection{Espressioni regolari}
Un'espressione regolare è una stringa che rappresenta un insieme di stringhe sull'alfabeto.
Le espressioni regolari sono definite induttivamente:
\begin{itemize}
	\item $\emptyset, \epsilon, (a \in \Sigma) \in RegExp$
	\item \textit{unione} di due espressioni regolari: \[\forall E_{1}, E_{2} \in RegExp \ \ \	E_{1} + E_{2} \in RegExp\]
	\item \textit{prodotto} o \textit{concatenazione} di due espressioni regolari: \[\forall E_{1}, E_{2} \in RegExp \ \ \ E_{1}E_{2} \in RegExp\]
	\item \textit{chiusura di Kleene} di un'espressione regolare:
		\[\mbox{definendo }E^{k} =
			\begin{cases}
				\epsilon, & \mbox{se } k = 0 \\
				E^{k-1} * E, & \mbox{se } k > 0 \\
			\end{cases}\]
		\[\forall E \in RegExp \ \ \ (E^{*} = \bigcup_{k \geq 0} E^{k}) \in RegExp\]
\end{itemize}

\textsf{Esempio}:\\
espressione regolare per le parole con numero di a pari
\[(b + ab^{*}a)^{*}\]
\newpage
\section{Esercizi a casa}
\subsection{Da automa a linguaggio}
Stabilire che linguaggio riconosce il seguente automa:
\begin{figure}[!ht]
	\centering
	\begin{tikzpicture}
		\node[state,initial,accepting] (q_0)	 {$q_0$}; 
		\node[state, accepting] (q_1) [right=of q_0] {$q_1$}; 
		\node[state] (q_2) [right=of q_1] {$q_2$};
		\path[->]
			(q_0) edge [loop below] node {$a$} (q_0)
			      edge node {$b$} (q_1)
			(q_1) edge [bend left = 20] node {$a$} (q_2)
			(q_2) edge [bend left = 20] node {$a$} (q_1);
	\end{tikzpicture}
\end{figure}

\textsf{Soluzione}:
\[L = \{ a^{n} \mid n \in \mathbb{N}\} \cup \{ a^{n}ba^{2m} \mid n,m \in \mathbb{N}\} \]
In particolare ciascuno stato contiene la seguente informazione:
\begin{itemize}
	\item $q_{0}$: non è stata ancora letta una $b$
	\item $q_{1}$: è stato letto un numero pari di $a$ dopo una $b$
	\item $q_{2}$: è stato letto un numero dispari di $a$ dopo una $b$
\end{itemize}

Espressione regolare corrispondente: $a^{*} (b(aa)^{*} + \epsilon)$

\subsection{Da linguaggio ad automa}
\label{sect:lez3-es2}
Costruire un automa per riconoscere il seguente linguaggio:
\[L = \{ x \in \{a, b\}^{*} \mid \mbox{ il terzultimo simbolo di \textit{x} è una }a\}\]

\textsf{Soluzione}:
\begin{figure}[!ht]
	\centering
	\hspace*{-1.5cm}
	\begin{tikzpicture}[initial where=above]
		\node[state,accepting] (aaa) {\textit{aaa}};
		\node[state,accepting] (aab) [right=of aaa] {\textit{aab}};
		\node[state,accepting] (abb) [below=of aaa] {\textit{abb}};
		\node[state] (baa) [below=of aab] {\textit{baa}};
		\node[state] (bab) [below=of baa] {\textit{bab}};
		\node[state] (bba) [below=of abb] {\textit{bba}};
		\node[state,accepting] (aba) [right=of baa] {\textit{aba}};
		\node[state, initial] (bbb) [left=of abb] {\textit{bbb}};
		\path[->]
			(aaa) edge [loop above] node {$a$} (aaa)
			      edge [pos=0.25] node {$b$} (aab)
			(aab) edge [pos=0.15] node {$a$} (aba)
			      edge [swap, pos=0.10] node {$b$} (abb)
			(aba) edge [swap, pos=0.25] node {$a$} (baa)
			      edge [bend left=20, pos=0.03] node {$b$} (bab)
			(abb) edge [swap, pos=0.25] node {$a$} (bba)
			      edge [swap, pos=0.25] node {$b$} (bbb)
			(baa) edge [pos=0.25, below] node {$a$} (aaa)
			      edge [swap, pos=0.25] node {$b$} (aab)
			(bab) edge [bend left=20, pos=0.14, left] node {$a$} (aba)
			      edge [swap, pos=0.10, above] node {$b$} (abb)
			(bba) edge [swap, pos=0.10, right] node {$a$} (baa)
			      edge [swap, pos=0.25] node {$b$} (bab)
			(bbb) edge [swap, pos=0.15] node {$a$} (bba)
			      edge [loop left] node {$b$} (bbb);
	\end{tikzpicture}
\end{figure}

Espressione regolare corrispondente: $(a+b)^{*}a(a+b)(a+b)$

Insiemisticamente: $\{a,b\}\{a,b\}a\{a+b\}^{*}$
