# Per contribuire
Pur essendo il progetto pubblico, soltanto poche persone hanno i permessi per modificare i file presenti.
Per contribuire al progetto si invita quindi a seguire le istruzioni sottostanti.

## Segnalare errori
[Creare una nuova issue](https://gitlab.com/MassimilianoPoggi/TeoriaLinguaggi/issues/new) spiegando l'errore trovato.

## Apportare modifiche
Seguire i seguenti passaggi:
1. [Creare un proprio fork del repository](https://gitlab.com/MassimilianoPoggi/TeoriaLinguaggi/forks/new)
2. Apportare le modifiche sul proprio fork
3. [Assicurarsi che il proprio fork sia aggiornato con le ultime modifiche](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)
4. [Aprire una merge request spiegando il cambiamento](https://docs.gitlab.com/ce/workflow/forking_workflow.html#merging-upstream)
